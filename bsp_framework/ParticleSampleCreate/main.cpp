#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <time.h>
#include <iomanip>
#include <limits>
#include <string>
#include <fstream>
using namespace std;
//
// ./a.out Num Folder
//
int main(int argc, char* argv[]){
    double *X, *Y;//粒子の初期位置の座標
    string *file_names;//粒子の初期位置を保存するファイル名
    string folder_name;
    int n;//初期粒子数

    n = atoi(argv[1]);
    folder_name = argv[2];

    X = (double *) malloc(n*sizeof(double));
    Y = (double *) malloc(n*sizeof(double));
    file_names = new string[n];

    //srand(12131);
    srand((unsigned)time(NULL));

    cout << setprecision(numeric_limits<double>::max_digits10) ;// 17
    double  xs,   ys;
    for(int j=0 ; j < n; j++) {
             xs = (double)rand()/(double)RAND_MAX;
             ys = (double)rand()/(double)RAND_MAX;
        X[j] = xs;   Y[j] = ys;
        file_names[j] = folder_name + "/" + to_string((int)floor(xs*100)) + "-" + to_string((int)floor(ys*100));//floorで小数点以下切り捨て、かつint型で0を削除
        //cout << j << ": " << file_names[j] << endl;
    }
    for(int i=0; i<n; i++){
        ofstream ofs(file_names[i], ios::app);
        //ofs << setprecision(numeric_limits<double>::max_digits10);
        //ofs << X[i] << "," << Y[i] << endl;
        ofs.write((const char*)&X[i], sizeof(double));
        ofs.write((const char*)&Y[i], sizeof(double));
    }
}