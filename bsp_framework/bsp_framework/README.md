#Overview
BSPモデルに基づいてCPU・GPU並列処理を行うフレームワークです。
C++に独自の並列関数の定義を書いたソースコードをpthread+CUDAのソースコードに書き換えます。
並列関数部分はCUDAによるGPU上の処理ができなければいけないため、C++のライブラリなどは使えないと思います。
ソースコードの書き換えはかなり雑な作りなので、分割されたコード等はうまくいかないかもしれません。
#Description
##スレッドの役割
全体としてはCPUの各スレッドが以下の役割を果たします。

* マスタスレッド
	* 処理を行うスレッドに対しての入力データの割り当てやマスタスレッド以外のスレッドの起動，同期を担当
* リード・ライトスレッド
	* 処理する入力データと各スレッドの作成したメッセージの ストレージに対しての読み書き
* デバイススレッド
	* デバイス(GPU)のメモリ確保，デバイスとのデータのやり取り，デバイスの処理の起動，同期
* ホストスレッド
	* 実際に処理

上から３種類のスレッドは各１スレッド、残りはホストスレッドとして処理を行います。

##元ソースコード書き方
以下で、defineします。
```
@bsp_define
{
	#define pi 3.14
	...
}
```

以下で並列処理関数を宣言します。
```
@bsp_func
{
	map;
	reduce;
}
```

並列処理関数とカウント関数を宣言した以降で定義してください。カウント関数の名前は上の例の場合``map_count`` ``reduce_count``のようにしてください。
また、引数はchar*を１つだけ取るようにしてください。

並列処理関数内ではデータの扱いがスーパーステップの初回と二回目以降で異なっています。
<dl>
	<dt>初回</dt>
	<dd>各スレッドはchar型の1本の配列として読み込んだデータを扱う形になります。各スレッドの担当範囲は'\0'で区切ってあるのでそこまで読ませるようにしてください。</dd>
	<dt>二回目以降</dt>
	<dd>各スレッドはメッセージとして各データを扱います。各メッセージのbodyはchar型の配列です。以下の関数を用いてメッセージを扱います。</dd>
</dl>

```
並列処理関数の引数名がchar*型strの場合

char* msg = BspFirstMsg(str); //最初のメッセージの本文へのポインタを返す
char* ID = BspGetIDPos(str); //メッセージの宛先となる文字列へのポインタを返す
int IDsize = BspGetIDSize(str); //メッセージの宛先となる文字列のサイズを返す

BspNextMsg(&msg); //msgが次のメッセージの本文を指すように変更
```

また、並列処理関数とカウント関数では以下のようにメッセージの送信、メッセージサイズの保存をします。
<dl>
	<dt>カウント関数</dt>
	<dd>``BspMemSetSize(int dist_id_size, int msg_size, int source_id_size);``</dd>
	ただし、dist_id_size, msg_size, source_id_sizeはそれぞれ送信先スレッドIDのサイズ、メッセージ本文のサイズ、送信元スレッドIDのサイズです。これを送信するメッセージの回数だけ呼び出してください。
	<dt>並列処理関数</dt>
	<dd>``BspSend( int dist_id_size, char* dist_id, int msg_size, char* msg, int source_id_size, char* source_id );``</dd>
	dist_id, msg, source_idのポインタからそれぞれのサイズだけコピーする形です。source_id_sizeとsource_idは省略可能です。
</dl>

並列処理関数の呼び出しは``map<<<>>>()``のように呼び出してください。

##ディレクトリ構成及びファイル説明
以下のようになってます。

* bsp 
	* 実際の処理に必要なもの
* code_gen
	* 元ソースコード　→　CUDAコード　の書き換えに必要なもの
* samples
	* サンプルとして作成したもの

###bsp
* bsp.hpp
	* ヘッダー
* method.cu
	* メソッド定義
* disk.cu
	* データのリード、ライトの関数定義
* device.cu
	* デバイススレッドの処理関数定義(GPUの処理の起動など)
* others.cu
	* その他関数定義(スレッドのスリープやbroadcastで起こす関数などはここ)
* debug.cu
	* デバッグ用マクロ定義に必要な関数(マクロ定義は都合上bsp.hppに書いてあります)

###code_gen
* code_gen.h
	* ヘッダー
* framework.cpp
	* ソースコードの生成部
* main.cpp
	* 入力受付
* tools.cpp
	* その他関数など、makefileの作成もここ

###samples
* particle
	* 粒子計算シミュレーション
* wordcount
	* 複数文書中の単語数のカウント

###コード生成によって作成されるもの
``samples/xxx/xxx.cpp``がソースのファイルとすると、

* samples/xxx/xxx.hpp
	* ヘッダー
* samples/xxx/xxx_func.cu
	* ユーザ定義の並列処理関数の定義
* samples/xxx/xxx_mod.cu
	* 元のファイルを書き換えたコード
* xxx_make
	* makefile

#Requirement
* CUDA8.0
* pthread
* Berkeley DB 6.2　(2018/02/28現在)

が必要です。また、最低でもCPU4スレッドは必要です。
#Usage
wordcountの例

```
make
./gen samples/wordcount/wordcount.cpp //コード生成
make -f wordcount_make
./wordcount ../dataset/1GB //実処理
```

#その他
分からない点は本人に聞いてください。