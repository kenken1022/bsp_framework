#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <unistd.h>
#include <algorithm>
#include <pthread.h>
#include <chrono>
#include <math.h>

using namespace std;

@bsp_define
{
	#define  N  (256*256);
	#define TAU 8.0;
	#define US(x, y, t) ( - 2.0*cos(M_PI*(t)/TAU)*sin(M_PI*(x))*sin(M_PI*(x))*cos(M_PI*(y))*sin(M_PI*(y)) );
	#define VS(x, y, t) ( 2.0*cos(M_PI*(t)/TAU)*cos(M_PI*(x))*sin(M_PI*(x))*sin(M_PI*(y))*sin(M_PI*(y)) );
}

@bsp_func
{
	ParticleCalc;
	ParticleCalcMsg;
}

ParticleCalc( char* str )
{
	char particle[20];
	char dist_name[20];
	int x_top = 0, 	y_top = 0;
	double x = 0, 	y = 0;
	double xt = 0, 	yt = 0;
	double xm = 0, 	ym = 0;
	double xn = 0, 	yn = 0;
	double time = 0;//初期段階は0
	double dt = 1.0/100.0;//時間は0.01で刻む
	char null = '\0';

	while( *str != '\0'){
		memcpy(&x, (double*)str, sizeof(double));
		memcpy(&y, (double*)(str+sizeof(double)), sizeof(double));
		str += 2*sizeof(double);

		xt = US(x,y,time);        yt = VS(x,y,time);
   		xm = x + 0.5*xt*dt;       ym = y + 0.5*yt*dt;
   		xn = x + xt*dt/6.0;       yn = y + yt*dt/6.0;
   		xt = US(xm, ym, time+0.5*dt);   yt = VS(xm, ym, time+0.5*dt);
   		xm = x + 0.5*xt*dt;          	ym = y + 0.5*yt*dt;
   		xn += xt*dt/3.0;             	yn += yt*dt/3.0;
   		xt = US(xm, ym, time+0.5*dt);   yt = VS(xm, ym, time+0.5*dt);
   		xm = x + xt*dt;              	ym = y + yt*dt;
   		xn += xt*dt/3.0;             	yn += yt*dt/3.0;
   		xt = US(xm, ym, time+dt);       yt = VS(xm, ym, time+dt);
   		xn += xt*dt/6.0;             	yn += yt*dt/6.0;

   		//x,y座標の0.xxxのx部分の組み合わせを次のスレッドの送り先とする
   		x_top = (int)floor(xn*500);
   		y_top = (int)floor(yn*500);

   		memcpy(dist_name, &x_top, sizeof(int));
   		memcpy(dist_name+sizeof(int), &y_top, sizeof(int));
   		// memcpy(dist_name+2*sizeof(int), &null, sizeof(char));

   		memcpy((double*)particle, &xn, sizeof(double));
   		memcpy((double*)(particle+sizeof(double)), &yn, sizeof(double));
   		memcpy((double*)(particle+2*sizeof(double)), &dt, sizeof(double));
   		// memcpy(particle+2*sizeof(double)+sizeof(float), &null, sizeof(char));

		BspSend( 2*sizeof(int), dist_name, 3*sizeof(double), particle );
		// BspSend( 2*sizeof(int)+1, dist_name, 2*sizeof(double)+sizeof(float)+1, particle );
	}
}

ParticleCalc_count( char* str )
{
	while( *str != '\0'){
   		BspMemSetSize( 2*sizeof(int), 3*sizeof(double), 0);
   		// BspMemSetSize( 2*sizeof(int)+1, 2*sizeof(double)+sizeof(float)+1, 0);
		str += 2*sizeof(double);
	}
}

ParticleCalcMsg( char* str )
{
	char particle[20];
	char dist_name[20];
	int x_top = 0, 	y_top = 0;
	double x = 0, 	y = 0;
	double xt = 0, 	yt = 0;
	double xm = 0, 	ym = 0;
	double xn = 0, 	yn = 0;
	double time = 0;
	double dt = 1.0/100.0;//時間は0.01で刻む
	char null = '\0';

	int arg_count=0;
	char* msg = BspFirstMsg(str);
	while( msg != NULL){
		memcpy(&x, (double*)(msg+sizeof(double)), sizeof(double));
		memcpy(&y, (double*)(msg+sizeof(double)+sizeof(double)), sizeof(double));
		memcpy(&time, (double*)(msg+2*sizeof(double)+sizeof(double)), sizeof(double));

		xt = US(x,y,time);        yt = VS(x,y,time);
   		xm = x + 0.5*xt*dt;       ym = y + 0.5*yt*dt;
   		xn = x + xt*dt/6.0;       yn = y + yt*dt/6.0;
   		xt = US(xm, ym, time+0.5*dt);   yt = VS(xm, ym, time+0.5*dt);
   		xm = x + 0.5*xt*dt;          	ym = y + 0.5*yt*dt;
   		xn += xt*dt/3.0;             	yn += yt*dt/3.0;
   		xt = US(xm, ym, time+0.5*dt);   yt = VS(xm, ym, time+0.5*dt);
   		xm = x + xt*dt;              	ym = y + yt*dt;
   		xn += xt*dt/3.0;             	yn += yt*dt/3.0;
   		xt = US(xm, ym, time+dt);       yt = VS(xm, ym, time+dt);
   		xn += xt*dt/6.0;             	yn += yt*dt/6.0;

   		x_top = (int)floor(xn*500);
   		y_top = (int)floor(yn*500);
   		time += dt;

   		memcpy(dist_name, &x_top, sizeof(int));
   		memcpy(dist_name+sizeof(int), &y_top, sizeof(int));
   		// memcpy(dist_name+2*sizeof(int), &null, sizeof(char));

		memcpy((double*)particle, &xn, sizeof(double));
   		memcpy((double*)(particle+sizeof(double)), &yn, sizeof(double));
   		memcpy((double*)(particle+2*sizeof(double)), &time, sizeof(double));
   		// memcpy(particle+2*sizeof(double)+sizeof(float), &null, sizeof(char));

		BspSend( 2*sizeof(int), dist_name, 3*sizeof(double), particle );
		// BspSend( 2*sizeof(int)+1, dist_name, 2*sizeof(double)+sizeof(float)+1, particle );
		
		BspNextMsg(&msg);
	}
}

ParticleCalcMsg_count( char* str )
{
	char* msg = BspFirstMsg(str);
	while( msg != NULL){		
   		BspMemSetSize( 2*sizeof(int), 3*sizeof(double), 0);
   		// BspMemSetSize( 2*sizeof(int)+1, 2*sizeof(double)+sizeof(float)+1, 0);
		BspNextMsg(&msg);
	}
}

string PreProcess(){
	string folder_name;
	cout<<"please choose folder"<<endl;
	cin>>folder_name;

	return folder_name;
}

void Calc( string folder_name ){
    int np = N,   icnt = 1,   numGPUs,  Lx = 512,  Ly = 512,  nout = 10;
	float time = 0.0;
    float dt = 1.0/100.0;
    char     filename[] = "f000.bmp";

    bool first = true;

	BspRead( folder_name );

	do{  
		if(icnt % 5 == 0) printf("time(%4d)=%7.5f\n",icnt,time + dt);
	
		if(first){
			ParticleCalc<<<>>>();
			first = false;
		}else{
			ParticleCalcMsg<<<>>>();
		}
		
		if(icnt % nout == 0) {
			// sprintf(filename,"f%03d.bmp",icnt/nout);
			// r8img_bmp(numGPUs,np,x,y,Lx,Ly,filename,"Seismic.pal");
		}
		time += dt;
	} while(icnt++ < 99 && time < 8.0 - 0.5*dt);

	return;
}

int main(int argc, char* argv[])
{
	string folder_name = argv[1];
	Calc( folder_name );

	printf("OK\n");
	return 0;
}