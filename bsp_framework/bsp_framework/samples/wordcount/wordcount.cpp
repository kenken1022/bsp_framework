#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>

#include <sstream>
#include <fstream>

#include <vector>
#include <unistd.h>
#include <algorithm>

#include <pthread.h>

#include <chrono>

using namespace std;

//必ず戻り値はvoidなので書いても書かなくてもよい？？
/*
 * 並列関数は必ず改行して列挙することにする（めんどいから）
 */
@bsp_define
{

}

@bsp_func
{
	map;
	reduce;
}

map_count( char* str )
{
	int key_size;
	key_size = 0;
	while( *str != '\0'){
		if( (*str >= 'a' && 'z' >= *str) || (*str >= 'A' && 'Z' >= *str) ){
			key_size++;
		}else if(key_size != 0){
			BspMemSetSize(key_size, (int)sizeof(int), 0);
			key_size = 0;
		}
		str++;
	}
}

map( char* str ){
	char* temp_pos = str;
	int cpy_size = 0;
	int number = 1;
	while( *str != '\0'){
		if(cpy_size == 0 && *str != ' ' && *str != '\t' && *str != '\r' && *str != '\n'){
			temp_pos = str;
		}
		if( (*str >= 'a' && 'z' >= *str) || (*str >= 'A' && 'Z' >= *str) ){
    		cpy_size++;
		}else if(cpy_size != 0){
			BspSend(cpy_size, temp_pos, (int)sizeof(int), (char*)&number);
			/*
			* BspSend(int size, void* source_id_pos, int size, void* dist_id_pos, int size, void* msg_pos)
			* or
			* BspSend(int size, void* dist_id_pos, int size, void* msg_pos)
			*/
			cpy_size = 0;
    	}
    	str++;
    	if(*str == '\0' && cpy_size != 0)BspSend(cpy_size, temp_pos, (int)sizeof(int), (char*)&number);
	}
}

reduce_count( char* str ){
	int IDsize = BspGetIDSize(str);
	BspMemSetSize(sizeof(int), sizeof(int), IDsize);
}

reduce( char* str ){
	int dist = 0;
	char* msg = BspFirstMsg(str);
	char* ID = BspGetIDPos(str);
	int IDsize = BspGetIDSize(str);
	int num = 0;
	while( msg != NULL){
		num++;
		BspNextMsg(&msg);
	}
	BspSend((int)sizeof(int), (char*)&dist, (int)sizeof(int), (char*)&num, IDsize, ID);
}

string PreProcess(){
	string folder_name;
	cout<<"please choose folder"<<endl;
	cin>>folder_name;

	return folder_name;
}

void Count( string folder_name ){
	BspRead( folder_name );
	map<<<>>>();
	//BspSendAssociate(map, reduce, reduce_num);
	reduce<<<>>>();

	return;
}

int main(int argc, char* argv[])
{
	//string folder_name = PreProcess();
	string folder_name = argv[1];
	Count( folder_name );

	printf("OK\n");
	return 0;
}