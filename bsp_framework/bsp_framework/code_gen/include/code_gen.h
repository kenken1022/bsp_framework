#ifndef CODE_GEN_H_
#define CODE_GEN_H_

#include <vector>
#include <string>
#include <string.h> //for "strstr"
#include <iostream>
#include <fstream>
#include <sstream>
#include <typeinfo>
#include <unistd.h>

using namespace std;

//tools.cpp//////////////////////////////////////////////////////////////////////////////////////////
int getCpuNum();
vector<string> SplitString(const string &s, char delim);
string GetTextInBrackets( const char* input, const char left_char, const char right_char );
string EraseBlank( string st );
vector<string> ExtractParallelFuncs( string );
vector<string> ExtractDefines( string );
void CreateMakefile(string);
//////////////////////////////////////////////////////////////////////////////////////////////////////

//framework.cpp/////////////////////////////////////////////////////////////////////////////////////
void Translate( ifstream*, ofstream*, string );
string FindBspFunc( ifstream* );
string FindBspReadFunc( ifstream* );
//////////////////////////////////////////////////////////////////////////////////////////////////////


#endif /* CODE_GEN_H_ */