#include "code_gen.h"

int main(int argc, char* argv[]){
	string ifname,ofname;

	///元ファイルの読み込み////////////////////////////////////////////////////
	if(argc == 1){
		cout << "ファイルを指定してください。" << endl;
		cin >> ifname;
	}else{
		ifname = argv[1];
	}
	ifstream ifs( ifname.c_str() );
	ofstream ofs;

	if( ifs.fail() ){
		cerr << "ファイルの読み込みに失敗しました" << endl;
		exit(-1);
	}else{
		ofname = SplitString(ifname, '.')[0] + "_mod.cu";
		ofs.open( ofname.c_str() );
		cout << ofname << "を作成しました" <<endl;
	}
	////////////////////////////////////////////////////////////////////////

	//ソースコードの作成
	Translate( &ifs, &ofs, ifname );

	//makefileの作成
	CreateMakefile(ifname);

	return 0;
}