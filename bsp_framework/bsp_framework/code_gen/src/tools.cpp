#include "code_gen.h"

int getCpuNum(){
  #ifdef __linux
    return sysconf(_SC_NPROCESSORS_CONF);
  #elif __MINGW32__
    SYSTEM_INFO info;
    GetSystemInfo( &info );

    return info.dwNumberOfProcessors;
  #endif
}

vector<string> SplitString(const string &s, char delim){
	vector<string> elems;
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
    if (!item.empty()) {
            elems.push_back(item);
        }
    }
    return elems;
}

string GetTextInBrackets( const char* input, const char left_char, const char right_char ){
	string text_in_bra;

	if( left_char == right_char ){
		while( *input != left_char ){
			input++;
			if( *input == '\0') return NULL;
		}
		//開くを発見
		while( *input != right_char ){
			text_in_bra += *input;
			input++;
			if( *input == '\0' ) return NULL;
		}
		//閉じるを発見
		return text_in_bra;
	}else{
		while( *input != left_char ){
			input++;
			if( *input == '\0') return NULL;
		}
		//開くを発見
		int n=1;
		input++;
		if( *input == right_char )return "void";
		while( n>0 ){
			text_in_bra += *input;
			input++;
			if( *input == left_char )n++;
			if( *input == right_char )n--;
			if( *input == '\0' ) return "a";
		}
		//閉じるを発見
		return text_in_bra;
	}
}

string EraseBlank( string st ){
	size_t c;
	while( ( c = st.find_first_of(" \t")) != string::npos){
		st.erase(c, 1);
	}
	while( (c = st.find_last_of(" \t")) != string::npos){
		st.erase(c, 1);
	}
	return st;
}

vector<string> ExtractParallelFuncs( string bsp_func ){
	vector<string> parallel_funcs, temp_strings;
	temp_strings = SplitString( SplitString( SplitString(bsp_func,'{')[1], '}')[0], ';');

	for(int i=0; i<temp_strings.size(); i++){
		parallel_funcs.push_back( EraseBlank( temp_strings[i] ) );
	}

	return parallel_funcs;
}

vector<string> ExtractDefines( string str ){
	vector<string> defines, temp_strings;
	if(str.find(';') != -1)temp_strings = SplitString( SplitString( SplitString(str,'{')[1], '}')[0], ';');

	for(int i=0; i<temp_strings.size(); i++){
		defines.push_back( temp_strings[i] );
	}

	return defines;
}

void InsertIntoFuncDefAddSentence(string* host_code, string* device_code, string replaced_st, string host_st, string device_st){
	size_t pos;
	pos = host_code->find(replaced_st);
	while( pos != string::npos ){
		host_code->insert(pos + replaced_st.size()+1, host_st);
		host_code->insert(pos + replaced_st.size(), "_host");
		pos = host_code->find(replaced_st, pos+replaced_st.size());
	}
	pos = device_code->find(replaced_st);
	while( pos != string::npos ){
		device_code->insert(pos + replaced_st.size()+1, device_st);
		device_code->insert(pos + replaced_st.size(), "_device");
		pos = device_code->find(replaced_st, pos+replaced_st.size());
	}
}
void InsertIntoFuncDef(string* host_code, string* device_code, string replaced_st){
	size_t pos;
	pos = host_code->find(replaced_st);
	while( pos != string::npos ){
		host_code->insert(pos + replaced_st.size(), "_host");
		pos = host_code->find(replaced_st, pos+replaced_st.size());
	}
	pos = device_code->find(replaced_st);
	while( pos != string::npos ){
		device_code->insert(pos + replaced_st.size(), "_device");
		pos = device_code->find(replaced_st, pos+replaced_st.size());
	}
}

void CreateMakefile(string ifname){
	ofstream ofs;
	string name;
	if(ifname.find('/') != string::npos) name = SplitString( SplitString(ifname, '.')[0], '/').back();
	else name = SplitString(ifname, '.')[0];
	ofs.open( name + "_make");

	//pthreadオプションはなくても大丈夫
	string str = 
		"# Makefile\n"
		"# -ldb_cxx for Berkeley DB\n"
		"# -dc for dumping\n\n"
		"NVFLAGS 	= -O2 -ldb_cxx-6.2 -std=c++11\n"
		"DDEBUG 	=\n"
		"TARGET 	= ./"+name+"\n\n"
		"INCLUDE 	= -I./bsp/include\n"
		"SRCDIR 	= ./bsp/src\n"
		"SOURCES 	= $(wildcard $(SRCDIR)/*.cu)\n"
		"OBJDIR 	= ./bsp/obj\n"
		"OBJECTS 	= $(addprefix $(OBJDIR)/, $(notdir $(SOURCES:.cu=.o)))\n"
		"DEPENDS   	= $(OBJECTS:.o=.d)\n\n"
		"SAMPLEDIR 	= ./samples/"+name+"\n"
		"SAMPLESOURCES = $(wildcard $(SAMPLEDIR)/*.cu)\n"
		"SAMPLEOBJECTS = $(addprefix $(SAMPLEDIR)/, $(notdir $(SAMPLESOURCES:.cu=.o)))\n"
		"SAMPLEDEPENDS = $(SAMPLEOBJECTS:.o=.d)\n\n"
		".Phony: all debug\n"
		"all debug: $(TARGET)\n"
		"debug: DFLAGS = -DDEBUG -g -G\n\n"
		"$(TARGET): $(OBJECTS) $(SAMPLEOBJECTS) $(LIBS)\n"
		"	nvcc $(NVFLAGS) $(DFLAGS) -o $@ $^\n"
		"$(OBJDIR)/%.o: $(SRCDIR)/%.cu\n"
		"	-mkdir -p $(OBJDIR)\n"
		"	nvcc -dc $(NVFLAGS) $(DFLAGS) $(INCLUDE) -o $@ -c $<\n"
		"$(SAMPLEDIR)/%.o: $(SAMPLEDIR)/%.cu $(OBJECTS)\n"
		"	nvcc -dc $(NVFLAGS) $(DFLAGS) $(INCLUDE) -o $@ -c $<\n\n"
		".Phony: clean\n"
		"clean:\n"
		"	-rm -f $(OBJECTS) $(SAMPLEOBJECTS) $(DEPENDS) $(SAMPLEDEPENDS) $(TARGET)\n"
	;
	
	ofs << str << endl;

	return;
}