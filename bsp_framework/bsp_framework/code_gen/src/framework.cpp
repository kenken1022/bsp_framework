#include "code_gen.h"

void ExpandParallelFuncDef( string, ofstream*, ofstream* );
void ExpandParallelFunc( string, ofstream* );
void InsertIntoFuncDefAddSentence(string*, string*, string, string, string);
void InsertIntoFuncDef(string*, string*, string);

string bsp_send_from_threads_num;
string processing_document;
bool is_first_bsp_function;
string before_func_name;

void InitialSetting(string* str, string dir_name){
	*str = 
		"\tCMaster master(" + dir_name + ");\n"
		"\tCHost host[cpu_processing_thread];\n"
		"\tfor(int i=0; i<cpu_processing_thread; i++){\n"
		"\t\thost[i].setDataFromMaster(&master);\n"
		"\t}\n"
		"\tCDevice device;\n"
		"\tdevice.setDataFromMaster(&master);\n"
		"\tpthread_t th[cpu_processing_thread];\n"
		"\tpthread_t gpu_th;\n"
		"\tpthread_t rw_th;\n";
}

void Translate( ifstream* ifs, ofstream* ofs, string ifname ){
	string str="", parallel_func_def="";
	int check_comment = 0;
	int check_bsp_func = 0;
	int check_define = 0;
	int check_brackets = 0;
	int check_parallel_func_def = 0;
	int check_output = 1;
	int check_main = 0;

	string bsp_func;
	string bsp_define;

	vector<string> parallel_funcs;
	vector<string> defines;

	is_first_bsp_function = true;

	string sample_name;
	if(ifname.find('/') != string::npos) sample_name = SplitString( SplitString(ifname, '.')[0], '/').back();
	else sample_name = SplitString(ifname, '.')[0];

	cout << "コード生成を開始します" << endl;
	*ofs <<	"//////////////////////////////////////////////////\n"
			"//generated lines\n"
			"#include \"bsp.hpp\"\n"
			"#include \""+sample_name+".hpp\"\n"
			"bool wake_host_flag 			= false;\n"
			"bool memory_flag 				= false;\n"
			"pthread_cond_t master_cond 		= PTHREAD_COND_INITIALIZER;\n"
			"pthread_cond_t host_cond 		= PTHREAD_COND_INITIALIZER;\n"
			"pthread_cond_t rw_cond 			= PTHREAD_COND_INITIALIZER;\n"
			"pthread_cond_t memory_cond			= PTHREAD_COND_INITIALIZER;\n"
			"pthread_mutex_t master_mutex  	= PTHREAD_MUTEX_INITIALIZER;\n"
			"pthread_mutex_t host_mutex  	= PTHREAD_MUTEX_INITIALIZER;\n"
			"pthread_mutex_t rw_mutex 		= PTHREAD_MUTEX_INITIALIZER;\n"
			"pthread_mutex_t memory_mutex  	= PTHREAD_MUTEX_INITIALIZER;\n"
			"pthread_mutex_t output_queue_mutex 	= PTHREAD_MUTEX_INITIALIZER;\n"
			"pthread_mutex_t input_id_num_mutex 	= PTHREAD_MUTEX_INITIALIZER;\n"
			"pthread_mutex_t malloc_mutex 		= PTHREAD_MUTEX_INITIALIZER;\n"
			"//////////////////////////////////////////////////\n" << endl;

	//_func.cuにユーザが定義したbsp並列処理関数を定義
	ofstream func_def_ofs;
	func_def_ofs.open( SplitString(ifname, '.')[0] + "_func.cu");
	cout << SplitString(ifname, '.')[0] << "_func.cuを作成しました" << endl;
	func_def_ofs << 
		"#include \"bsp.hpp\"\n" 
		"#include \""+sample_name+".hpp\"\n"
	<< endl;

	//ヘッダー
	ofstream func_hpp_ofs;
	func_hpp_ofs.open( SplitString(ifname, '.')[0] + ".hpp");
	cout << SplitString(ifname, '.')[0] << ".hppを作成しました" << endl;
	func_hpp_ofs << 
		"#ifndef bsp_sample_hpp_\n"
		"#define bsp_sample_hpp_\n\n"
		"#include \"bsp.hpp\"\n"
		"#define cpu_processing_thread " << getCpuNum() - 3 << "\n"
	<< endl;//マスタスレッド、デバイススレッド、ディスクスレッドを抜かすCPUスレッド数がホストスレッド

	while( getline( *ifs, str ) ){
		int i=0;
		check_output = 1;

		//コメントアウト処理///////////////////////////////////////////////////////////////////
		if( check_comment == 1 ){
			if( str.find("*/") == -1 ){
				str.erase();
			}else{
				str.erase( 0, str.find("*/")+2 );
				check_comment--;
			}
		}else if( check_comment == 0 ){
			if( str.find("//") != -1){
				str.erase( str.find("//") );
			}else if( str.find("/*") != -1 && str.find("*/") != -1 ){
				str.erase( str.find("/*"), str.find("*/")+2 - str.find("/*") );
			}else if( str.find("/*") != -1){
				str.erase( str.find("/*") );
				check_comment++;
			}
		}
		///////////////////////////////////////////////////////////////////////////////

		if( str.empty() ){
			check_output = 0;
		}else{
			///////////////////////////////////////////////////////////////////////////////
			/*
			 * mainの先頭にMemoryCheckを起動させるようにする
			 */
			if( (str.find("void") != -1 || str.find("int") != -1) &&
				str.find("main") != -1 && str.find(";") == -1){
				if(str.find("{") != -1){
					//str.insert(str.find("{")+1, "\n\tMemoryCheck();\n");
				}else{
					check_main = 1;
				}
			}
			if( str.find('{') != -1 ){
				if( check_main == 1 ){
					//str.insert(str.find("{")+1, "\n\tMemoryCheck();\n");
					check_main = 0;
				}
				//{の何層目にいるのか
				check_brackets++;
			}
			if( str.find('}') != -1 ){
				check_brackets--;
			}
			///////////////////////////////////////////////////////////////////////////////


			///////////////////////////////////////////////////////////////////////////////
			if( check_bsp_func == 2 ){
				/*
			 	 * check_bsp_funcが2のとき、既に並列関数の宣言は終わっている	
				 * そのため、各行を確認しながら並列関数の定義の部分を探す（something{}があるところ）
				 */
				if( str.find('}') != -1 && check_define == 2){
					if(check_brackets == 0 && check_parallel_func_def == 1){
						check_output = 0;
						parallel_func_def += str;
						check_parallel_func_def--;
						ExpandParallelFuncDef( parallel_func_def, &func_def_ofs, &func_hpp_ofs );
						parallel_func_def.erase();
					}
				}

				/*
			 	 * parallel_funcsに並列関数名を保存する
			 	 * check_bsp_funcが2になっているときには並列関数の宣言が終わっているので
			 	 * 	各行に並列関数を呼び出す記述がないかどうか確認する
			 	 */
				for(int j=0; j<parallel_funcs.size(); j++){
					//今見ている1行の中に並列関数の名前と一致するものがあるかどうか
					if( EraseBlank( SplitString( str, '(' )[0] ) == parallel_funcs[j] 
						|| EraseBlank( SplitString( str, '(' )[0] ) == parallel_funcs[j] + "_count"){
						check_parallel_func_def++;
					//並列関数の呼び出し式はExpandParallelFuncを用いてホスト用とデバイス用に呼び出しを分ける
					}else if( str.find( parallel_funcs[j] + "<<<>>>" ) != -1){
						ExpandParallelFunc( str, ofs );
						check_output = 0;
					}
				}
			}
			///////////////////////////////////////////////////////////////////////////////


			///////////////////////////////////////////////////////////////////////////////
			/*
			 * もし@bsp_func{ }で囲まれた中ならば、その関数（各行）はbsp_funcに加える
			 */
			if( str.find("@bsp_func") != -1 ){
				check_output = 0;
				check_bsp_func = 1;
				bsp_func += str;
			}else if( check_bsp_func == 1 ){
				check_output = 0;
				bsp_func += str;
			
				//すべてのbsp_funcを読み終えたら、各関数のホスト用定義を行う
				if( str.find('}') != -1 ){
					parallel_funcs = ExtractParallelFuncs( bsp_func );
					*ofs << endl;
					for(int j=0; j < parallel_funcs.size(); j++){
						*ofs << "void* " << parallel_funcs[j] << "_host(void* tag);" << endl;
					}
					*ofs << endl;
					check_bsp_func = 2;
				}
			}
			///////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////
			if( str.find("@bsp_define") != -1){
				check_output = 0;
				check_define = 1;
				bsp_define += str;
			}else if ( check_define == 1 ){
				check_output = 0;
				bsp_define += str;

				if( str.find('}') != -1 ){
					defines = ExtractDefines( bsp_define );
					func_hpp_ofs << endl;
					for(int j=0; j < defines.size(); j++){
						func_hpp_ofs << defines[j] << endl;
					}
					func_hpp_ofs << endl;
					check_define = 2;
				}
			}

			///////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////
			/*
			 * 並列関数の定義式を1本のstringにまとめて、ExpandParallelFuncDefで
			 * 	ホスト用とデバイス用に書き換える
			 */
			if( check_parallel_func_def == 1){
				check_output = 0;
				parallel_func_def = parallel_func_def + str + '\n';
			}
			///////////////////////////////////////////////////////////////////////////////


			//BspReadに自動生成したCFolder, CParallelProcessを引数として渡す/////////////
			if( str.find("BspRead") != -1 ){
				processing_document = EraseBlank( SplitString(SplitString( str, '(' )[1], ')')[0] );
				InitialSetting(&str, processing_document);
			}
			////////////////////////////////////////////////////////////////////////////////
		}

		//check_outputが0のときには書き込みを行わない
		if( check_output != 0 )*ofs << str << endl;
	}
	func_hpp_ofs << "#endif /* bsp_sample_hpp_ */" << endl;
}

void ExpandParallelFuncDef( string parallel_func_def, ofstream* func_def_ofs, ofstream* func_hpp_ofs ){
	string host_code = parallel_func_def;
	string device_code = parallel_func_def;
	string char_pointer_arg;
	string int_pointer_arg = "input_stream_offset";
	
	//関数名の前にある空白、及びtabを削除
	string func_name = EraseBlank( SplitString( parallel_func_def, '(' )[0] );

	//関数から引数を抜き出す
	string args_host = SplitString( SplitString( 
								parallel_func_def, ')' )[0], '(' )[1];

	size_t  pos;
	while( (pos = args_host.find_first_of("   \t") ) != string::npos )args_host.erase(pos,1);
	while( (pos = args_host.find_last_of("   \t") ) != string::npos )args_host.erase(pos,1);
	if( SplitString(args_host, '*')[0] == "char" ){
		char_pointer_arg = SplitString(args_host, '*')[1];
	}else if( SplitString(args_host, '*')[0] != "char" ){
		cout<<"funtion arguments error"<<endl;
		exit(-1);
	}
	
	//ホスト用とデバイス用それぞれについて必要な部分を添加
	host_code.erase( host_code.find_first_of( '(' )+1, host_code.find_first_of( ')' ) - host_code.find_first_of( '(' ) - 1 );
	host_code.insert( 0, "__host__ void* " );
	host_code.insert( host_code.find_first_of( '(' ), "_host" );
	host_code.insert( host_code.find_first_of( '(' )+1, "void* tag" );
	host_code.insert( host_code.find_first_of( '{' )+1, 
		"\n"
		"\tauto start = std::chrono::system_clock::now();\n"
		"\tCHost* host 	= (CHost*)tag;\n"
		"\tfor(int iteration=0; iteration<host->m_num_total_threads; iteration++){\n"
		"\tchar* " + char_pointer_arg + " 	= &host->m_stream[host->m_offset[iteration]-host->m_offset_standard];\n\n"
		);
	host_code.insert( host_code.find_last_of( '}' )-1, 
		"}\n" 
		"\tpushOutputDataToQueueHost(host);\n"
		"\twakeMasterUp(&host->m_end_flag);\n"
		"\tauto end = std::chrono::system_clock::now();\n"
    	"\tauto diff = end - start;\n"
    	"\thost->m_processing_time += std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();\n"

		);
	string func_name_without_count = func_name;
	if(func_name.find("_count") != string::npos){
		func_name_without_count.erase( func_name.find("_count"), 6);
	}
	/////////////////////////////////////////////////////////////////////////////////////////
	device_code.insert( 0, "__global__ void " );
	device_code.insert( device_code.find_first_of( '(' ), "_device" );
	if( func_name.find("_count") == string::npos){
		device_code.insert( device_code.find_first_of( '{' )+2,
		 "\tint device_tid = blockDim.x*blockIdx.x+threadIdx.x;\n\tif("+int_pointer_arg+"[device_tid] != -1){\n"
		 "\t" + char_pointer_arg +" += " + int_pointer_arg +"[device_tid] - offset_standard;\n"
		 "\toutput_stream += output_size_offset[device_tid] - output_offset_standard;\n\n"
		 );
		device_code.insert( device_code.find_first_of( ')' ), ",size_t* input_stream_offset, char* output_stream, size_t* output_size_offset");
		device_code.insert( device_code.find_last_of( '}' ), "}");
	}else{
		device_code.insert( device_code.find_first_of( '{' )+2,
		 "\tint device_tid = blockDim.x*blockIdx.x+threadIdx.x;\n\tif("+int_pointer_arg+"[device_tid] != -1){\n"
		 "\t" + char_pointer_arg +" += "+ int_pointer_arg +"[device_tid] - offset_standard;\n\n"
		 );
		device_code.insert( device_code.find_first_of( ')' ), ",size_t* input_stream_offset, int* output_size_offset, int* output_num");
		device_code.insert( device_code.find_last_of( '}' ), "}");
	}

	//各キーワードについて変更を加える
	InsertIntoFuncDefAddSentence(&host_code, &device_code, "BspMemSetSize", " ", "output_size_offset + device_tid, output_num + device_tid,");//hostにはこの関数なし
	InsertIntoFuncDefAddSentence(&host_code, &device_code, "BspSend", "host, ", "&output_stream, ");
	InsertIntoFuncDef(&host_code, &device_code, "BspGetIDPos");
	InsertIntoFuncDef(&host_code, &device_code, "BspGetIDSize");
	InsertIntoFuncDef(&host_code, &device_code, "BspFirstMsg");
	InsertIntoFuncDef(&host_code, &device_code, "BspNextMsg");

	if(func_name.find("_count") == string::npos){
		*func_def_ofs << host_code << endl;

		//countの関数定義じゃない場合にヘッダーにこれらを作成
		*func_hpp_ofs << 
			"extern __host__ void* " << func_name << "_host(void*);\n"
			"extern __global__ void " << func_name << "_count_device(char*, size_t*, int*, int*);\n"
			"extern __global__ void " << func_name << "_device(char*, size_t*, char*, size_t*);\n"
		<< endl;
	}
	*func_def_ofs << device_code << endl;

	return;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * ExpandParallelFunc:関数呼び出しの部分を展開する
 * 展開はホスト用の部分とデバイス用の部分に分けられる
 * その際にホスト用の構造体の用意やデバイス用のデバイスメモリの確保、データ送信も行う
 */
void ExpandParallelFunc( string str, ofstream* ofs ){
	//関数名の前にある空白、及びtabを削除
	string func_name = EraseBlank( SplitString( str, '<' )[0] );
	vector<string> func_args = SplitString( SplitString( SplitString( str, '(')[1], ')' )[0], ',' );

	string border_start =
		"//---------"+func_name+" start----------------------------------------------------------------------\n";
	*ofs<< border_start << endl;

	//メインループの処理の前段階
	string processing_str = 
	"auto "+func_name+"_start = std::chrono::system_clock::now();\n"
	"pthread_mutex_lock( &master_mutex );\n"
	"pthread_create( &rw_th, NULL, BspRead, &master );\n"
	"while(!master.m_rw_flag)pthread_cond_wait( &master_cond, &master_mutex );\n"
	"master.m_rw_flag 	= false;\n"
	"pthread_join( rw_th, NULL );\n"
	"pthread_create( &rw_th, NULL, BspRead, &master );\n"
	"device.setFunc("+func_name+"_count_device, "+func_name+"_device);\n"
	"for(int i=0; i<cpu_processing_thread; i++){\n"
	"	if(master.needsProcess()){\n"
	"		master.setHostProcessData(&host[i]);\n"
	"		pthread_create( &th[i], NULL, "+func_name+"_host, &host[i]);\n"
	"	}else{\n"
	"		wake_host_flag = false;\n"
	"		pthread_create( &th[i], NULL, hostSleep, &host[i] );\n"
	"	}\n"
	"}\n"
	"if(master.needsProcess()){\n"
	"	master.setDeviceProcessData(&device);\n"
	"	pthread_create( &gpu_th, NULL, deviceProcessing, &device);\n"
	"}else{\n"
	"	wake_host_flag = false;\n"
	"	pthread_create( &gpu_th, NULL, hostSleep, &device );\n"
	"}\n"
	;

	//メインの処理を行うループ
	processing_str +=
	"while( !master.hasFinished() ){\n"
	"while(\n"
		;
	for(int i=0; i<getCpuNum()-3; i++){
		processing_str +=
		"!host["+to_string(i)+"].m_end_flag &&\n"
		;
	}
	processing_str += 
	"!device.m_end_flag &&\n"
	"!master.m_rw_flag\n"
	")pthread_cond_wait( &master_cond, &master_mutex );\n"
	"if(master.hasFinished()){\n"
	;
	for(int i=0; i<getCpuNum()-3; i++){
		processing_str +=
		"}else if(host["+to_string(i)+"].m_end_flag){\n"
		"	pthread_join( th["+to_string(i)+"], NULL );\n"
		"	if(host["+to_string(i)+"].m_last_thread){\n"
		"		pthread_mutex_lock(&output_queue_mutex);\n"
		"		master.m_process_end_flag = true;\n"
		"		pthread_mutex_unlock(&output_queue_mutex);\n"
		"	}\n"
		"	if(!master.m_free_queue.empty())master.freeInput();\n"
		"	host["+to_string(i)+"].m_end_flag = false;\n"
		"	if(master.needsProcess()){\n"
		"		master.setHostProcessData( &host["+to_string(i)+"] );\n"
		"		pthread_create( &th["+to_string(i)+"], NULL, "+func_name+"_host, &host["+to_string(i)+"] );\n"
		"	}else{\n"
		"		wake_host_flag = false;\n"
		"		pthread_create( &th["+to_string(i)+"], NULL, hostSleep, &host["+to_string(i)+"] );\n"
		"	}\n"
		;
	}
	processing_str +=
	"}else if(device.m_end_flag){\n"
	"	pthread_join( gpu_th, NULL );\n"
	"	if(device.m_last_thread){\n"
	"		pthread_mutex_lock(&output_queue_mutex);\n"
	"		master.m_process_end_flag = true;\n"
	"		pthread_mutex_unlock(&output_queue_mutex);\n"
	"	}\n"
	"	if(!master.m_free_queue.empty())master.freeInput();\n"
	"	device.m_end_flag = false;\n"
	"	if(master.needsProcess()){\n"
	"		master.setDeviceProcessData( &device );\n"
	"		pthread_create( &gpu_th, NULL, deviceProcessing, &device);\n"
	"	}else{\n"
	"		wake_host_flag = false;\n"
	"		pthread_create( &gpu_th, NULL, deviceSleep, &device );\n"
	"	}\n"
	"}else if(master.m_rw_flag ){\n"
	"	if(master.hasFinished())break;\n"
	"	pthread_join( rw_th, NULL );\n"
	"	master.m_rw_flag = false;\n"
	"	if(master.needsRead()){\n"
	"		pthread_create( &rw_th, NULL, BspRead, &master );\n"
	"	}else{\n"
	"		pthread_create( &rw_th, NULL, TempDBInsert, &master );\n"
	"	}\n"
	"}}\n"
	;

	//メインループ終了後の後段階
	processing_str +=
	"wake_host_flag = true;\n"
	"pthread_cond_broadcast(&host_cond);\n"
	"pthread_mutex_unlock( &master_mutex );\n"
	"for(int i=0; i<cpu_processing_thread; i++)pthread_join( th[i], NULL );\n"
	"pthread_join( gpu_th, NULL );\n"
	"pthread_join( rw_th, NULL );\n"
	"master.m_rw_flag 	= false;\n"
	"master.shiftNextSS();\n"
	"auto "+func_name+"_end = std::chrono::system_clock::now();\n"
	"auto "+func_name+"_diff = std::chrono::duration_cast<std::chrono::milliseconds>("+func_name+"_end-"+func_name+"_start).count();\n"
	"for(int i=0; i<cpu_processing_thread; i++)cout << \"host\" << i << \": \" <<host[i].m_processing_time << endl;\n"
	"cout << \"device: \" << device.m_processing_time << endl;\n"
	"cout << \"read: \" << master.m_read_time << endl;\n"
	"cout << \"write: \" << master.m_write_time << endl;\n"
	"cout << \""+func_name+": \" << "+func_name+"_diff << endl;\n"
	"for(int i=0; i<cpu_processing_thread; i++)host[i].shiftNextSS();\n"
	"device.shiftNextSS();\n"
	"master.m_read_time = 0;\n"
	"master.m_write_time = 0;\n"
	"wake_host_flag = false;\n"
	;

	*ofs << processing_str << endl;

	string border_end =
		"//---------"+func_name+" end----------------------------------------------------------------------\n";
	*ofs<< border_end << endl;

	return;
}
