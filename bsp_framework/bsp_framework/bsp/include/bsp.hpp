#ifndef BSP_H_
#define BSP_H_

#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <unistd.h>
#include <db_cxx.h>//Berkeley DB
#include <chrono>//時間計測
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <utility>
#include <queue>
#include <iomanip>
#include <stdint.h>
#include <exception>
#include <new>
#include <map>

#include <stdarg.h> //デバッグ可変長引数用

using namespace std;

class CMemory;
class CFolder;
class CDBset;
class CDB;
class CMaster;
class CHost;
class CDevice;
class CHostdata;
class CDevicedata;
class CCountData;

#define ALLOWANCE             1024*1024*5
#define DEFAULT_THREAD_NUM   100
#define DEFAULT_THREAD_NUM_DEVICE 1536
#define DEFAULT_OUTPUT_SIZE   1024*1024//B
#define TEST_CPU_MEMORY       1000//MB
#define TEST_GPU_MEMORY       500//MB

// extern int DEFAULT_THREAD_NUM;

extern CMemory cpu_memory;
extern CMemory gpu_memory;

extern __device__ __constant__ size_t offset_standard;
extern __device__ __constant__ size_t output_offset_standard;

//extern int BspRead( std::string, char**, int**, DIR* dp=NULL, dirent* entry=NULL, int reading_position=0 );
//extern void BspReadInfoGet( std::string );
//extern int BspRead( CFolder*, CParallelProcess*, std::string, int* output_size = NULL, int* output_num = NULL );
extern void BspReadForCount( size_t available_memory, CFolder* f_data, CHostdata* h_data);
extern void BspReadForCountToGPU( size_t available_memory, CHostdata* h_data, CDevicedata* d_data );
extern int BspReadInfoGet( std::string, int*);
extern void SaveCountDataToDB(Db*, size_t, unsigned int, unsigned int, int*, int* );
//extern void BspRead( size_t available_memory, CFolder* f_data, CHostdata* h_data ,CCountData* c_data);
extern void* BspRead(void* arg);
extern void BspReadToGPU( size_t available_memory, CHostdata* h_data, CDevicedata* d_data );
extern void MemoryCheck();
extern int getCpuNum();
extern void* TempDBInsert( void* );
extern void* TempDBSelectForCount( void* tag );
extern void* TempDBSelectForCountToGPU( void* tag );
extern string poi_to_st(void* pointer);

extern void* deviceProcessing(void* arg);
extern void* hostSleep(void* arg);
extern void* deviceSleep(void* arg);
extern void wakeMasterUp(bool* flag);
extern void wakeHostUp();
extern void pushOutputDataToQueueHost(CHost* host);
extern void pushOutputDataToQueueDevice(CDevice* device);
extern __host__ void BspMemSetSize_host(int* output_size_buf,
                                    int* output_num_buf,
                                    int dist_id_size,
                                    int source_id_size, 
                                    int msg_size);
extern __device__ void BspMemSetSize_device(int* output_size_buf,
                                    int* output_num_buf,
                                    int dist_id_size,
                                    int source_id_size, 
                                    int msg_size);
extern __host__ void BspSend_host(CHost* host, 
                              int dist_id_size, char* dist_id,
                              int msg_size, char* msg,
                              int source_id_size=0, char* source_id=NULL);
extern __device__ void BspSend_device(char** output_stream, 
                              int dist_id_size, char* dist_id,
                              int msg_size, char* msg,
                              int source_id_size=0, char* source_id=NULL);
extern __host__ int BspGetIDSize_host(char* str);
extern __device__ int BspGetIDSize_device(char* str);
extern __host__ char* BspGetIDPos_host(char* str);
extern __device__ char* BspGetIDPos_device(char* str);
extern __host__ char* BspFirstMsg_host(char* str);
extern __device__ char* BspFirstMsg_device(char* str);
extern __host__ void BspNextMsg_host(char** msg_pointer);
extern __device__ void BspNextMsg_device(char** msg_pointer);
extern __host__ int BspTid_host();
extern __device__ int BspTid_device();
extern __device__ void CUDAsleep(int clock_num);
extern __host__ void db_check(Db*);
extern __host__ int compare_int(Db *db, const Dbt *a, const Dbt *b, size_t *locp);

//
// デバッグ用
// 
extern pthread_mutex_t debug_queue_mutex;
extern queue<string> argment_contents;

extern void set_args_con();
template<class First, class... Rest>
void set_args_con(const First& first, const Rest&... rest) {
    stringstream ss;
    ss<<first;
    pthread_mutex_lock(&debug_queue_mutex);
    argment_contents.push(ss.str());
    pthread_mutex_unlock(&debug_queue_mutex);
    set_args_con(rest...);
}
extern string gen_string(string s);

#ifdef DEBUG
#define dump(...) {set_args_con(__VA_ARGS__);cerr<<gen_string(#__VA_ARGS__)<<endl;}
#else
#define dump(...)
#endif /* DEBUG */
//
// デバッグ用
// 

#define CUDA_SAFE_CALL(func) \
do { \
     cudaError_t err = (func); \
     if (err != cudaSuccess) { \
         fprintf(stderr, "[Error] %s (error code: %d) at %s line %d\n", cudaGetErrorString(err), err, __FILE__, __LINE__); \
         exit(err); \
     } \
} while(0);

#define SAFE_MALLOC(pointer, type, size) \
do{ \
      pthread_mutex_lock(&malloc_mutex); \
      pointer = (type)malloc(size); \
      if(pointer == NULL){ \
            memory_flag = false; \
            while(!memory_flag)pthread_cond_wait( &memory_cond, &malloc_mutex); \
      }else{ \
            memset(pointer, 0, size); \
      } \
      pthread_mutex_unlock(&malloc_mutex); \
}while(pointer == NULL);

#define SAFE_NEW(pointer, type, size) \
do{ \
      pthread_mutex_lock(&malloc_mutex); \
      pointer = new type[size]; \
      if(pointer == NULL){ \
            memory_flag = false; \
            while(!memory_flag)pthread_cond_wait( &memory_cond, &malloc_mutex); \
      } \
      pthread_mutex_unlock(&malloc_mutex); \
}while(pointer == NULL);

#define SAFE_FREE(pointer) \
do{ \
      free(pointer); \
      pointer = NULL; \
      pthread_mutex_lock(&malloc_mutex); \
      pthread_cond_broadcast(&memory_cond); \
      pthread_mutex_unlock(&malloc_mutex); \
}while(0);

#define SAFE_DELETE(pointer) \
do{ \
      delete[] pointer; \
      pointer = NULL; \
      pthread_mutex_lock(&malloc_mutex); \
      pthread_cond_broadcast(&memory_cond); \
      pthread_mutex_unlock(&malloc_mutex); \
}while(0);

struct tagINPUT{
      char*             stream;
      size_t*           offset;
      unsigned int      thread_num;
};

struct tagOUTPUT{
      unsigned int      output_memory_id;
      char*             stream;
      size_t            size;
};

struct tagFREE{
      tagINPUT          input_data;
      int               id;
};

extern int memory_num;
extern bool wake_host_flag;
extern bool memory_flag;
extern queue<string> argment_contents;

extern pthread_cond_t master_cond;
extern pthread_cond_t host_cond;
extern pthread_cond_t rw_cond;
extern pthread_cond_t memory_cond;
extern pthread_mutex_t master_mutex;
extern pthread_mutex_t host_mutex;
extern pthread_mutex_t rw_mutex;
extern pthread_mutex_t memory_mutex;
extern pthread_mutex_t output_queue_mutex;
extern pthread_mutex_t input_id_num_mutex;
extern pthread_mutex_t malloc_mutex;

class CMemory{
      unsigned int                              m_serial_num;
      size_t                                    m_max_memory;
      size_t                                    m_current_memory;
      std::unordered_map<void*, size_t>*        m_memory_consumption;
      std::unordered_map<unsigned int, size_t>* m_output_memory_consumption;
public:
      CMemory();
      void setMemoryMax( size_t max );
      bool isAvailable( size_t memory);
      bool useMemory( void* pointer, size_t memory );
      bool useMemoryForOutput( unsigned int id, size_t memory);
      bool addMemory( void* pointer, size_t memory );
      bool freeMemory( void* pointer );
      bool freeMemoryForOutput( unsigned int id);
      size_t getInfo();
      unsigned int getSerialNum();
      ~CMemory();
};
/*
//もともとの実装
class CDBset{
public:
      unsigned int      m_serial_num;
      int               m_db_num;
      DbEnv             m_rmEnv;
      DbEnv             m_myEnv;
      Db                m_temp_db0;
      Db                m_temp_db1;
      Db*               m_db_read;
      Db*               m_db_write;
      Dbc*              m_cursor;
      
      CDBset();
      void shiftNextSS(){
            m_serial_num      = 0;

            Db* temp_dbp      = m_db_read;
            m_db_read         = m_db_write;
            m_db_write        = temp_dbp;

            // u_int32_t count = 0;
            // m_db_write->truncate(NULL, &count, 0);
            // cout << "num of deleted items: " << count << endl;
      }
      ~CDBset();
};
*/

class CDB{
public:
      Db db;

      CDB():db(NULL, 0)
      {
      }
      CDB(DbEnv* env):db(env, 0)
      {
      }
};

class CDBset{
public:
      unsigned int      m_serial_num;
      int               m_db_num;
      DbEnv             m_rmEnv;
      DbEnv             m_myEnv;
      CDB               m_temp_db[100];
      Db*               m_db_read;
      Db*               m_db_write;
      Dbc*              m_cursor;
      
      CDBset();
      void shiftNextSS(){
            m_serial_num      = 0;


            if(m_db_num != 0){
                  // m_temp_db[m_db_num-1].db.close(0);
                  // string db_name = "/etc/db/temp_db"+to_string(m_db_num-1)+".db";
                  // m_rmEnv.dbremove(NULL, db_name.c_str(), NULL, 0);
                  // remove(db_name.c_str());
            }
            m_db_read         = &(m_temp_db[m_db_num].db);
            m_db_write        = &(m_temp_db[++m_db_num].db);
      }
      ~CDBset();
};


class CFolder{
public:
      long m_dpoffset;
      DIR* m_dp;
      dirent* m_entry;
      std::string folder_name;

      CFolder();
      CFolder(std::string folder_name);
      void Clear();
      bool needsOpen();
      bool Open();
      ~CFolder();
};

class CMaster{
public:
      unsigned int      m_current_position;//現在の読み込みデータ中でのスレッドの位置
      unsigned int      m_current_position_overall;//全体の読み込みデータ中でのスレッドの位置(複数回に分割してデータ読み込む)
      unsigned int      m_current_input_memory_size;
      unsigned int      m_current_input_thread_size;
      int               m_input_id;
      double            m_allocation_host_time;
      double            m_allocation_device_time;
      double            m_read_time;
      double            m_write_time;
      size_t            m_current_stream_offset;
      size_t            m_max_stream_size;
      bool              m_priority_writing_flag;
      bool              m_read_end_flag;
      bool              m_process_end_flag;
      bool              m_write_end_flag;
      bool              m_rw_flag; //読み書き用
      bool              m_needs_file_read; //ファイルからデータを読み込むかDBから読み込むか
      CFolder           m_folder;
      CDBset            m_db;
      CMemory*          m_cpu_memory;
      CMemory*          m_gpu_memory;
      std::queue< tagINPUT >  m_input_queue;//inputデータの先頭アドレスとオフセット
      std::queue< tagOUTPUT > m_output_queue;//outputのメモリの先頭アドレスとそのサイズを保持
      std::queue< tagFREE >   m_free_queue;//データの割り当てを済ませたinputデータをこっちに移す
      std::map<int, unsigned int> m_input_id_num;//input idをキー、そのinputを処理するスレッド数がデータ
      std::map<int, bool>     m_input_id_finished;//input idをキー、そのinputを割り当て終えてるかどうかがデータ

      CMaster(std::string folder_name);
      bool needsRead(){return !m_read_end_flag && !m_priority_writing_flag;}
      bool needsProcess(){return !m_input_queue.empty();}
      bool hasFinished(){return     (this->m_read_end_flag &&
                                    this->m_process_end_flag &&
                                    this->m_write_end_flag);}
      void MemorySet();
      void shiftNextSS();
      void setHostProcessData(CHost* host);
      void setDeviceProcessData(CDevice* device);
      void freeInput();
      void setNextInput(unsigned int);
      void setSmallerInput();


      ~CMaster(){
            SAFE_DELETE(m_cpu_memory);
            SAFE_DELETE(m_gpu_memory);
      }
};

class CHost{//各CPU処理スレッドがこのクラスを作成？？
public:
      unsigned int      m_current_position_overall;
      unsigned int      m_num_total_threads;
      unsigned int      m_output_memory_id;
      int               m_input_id;
      double            m_processing_time;
      size_t            m_total_stream_size;
      size_t            m_current_output_size;
      size_t            m_total_output_size;
      size_t            m_offset_standard;
      bool              m_end_flag;//pthreadのフラグ
      bool              m_last_thread;//スーパーステップの最後のスレッド
      char*             m_stream;
      size_t*           m_offset;
      char*             m_output_stream;
      std::queue< tagOUTPUT >*
                        m_output_queue;//アウトプットの先頭アドレスとそのサイズ
      std::map<int, unsigned int>*
                        m_input_id_num;//idをキー、その個数がデータ
      CMemory*          m_cpu_memory;

      CHost();
      void setDataFromMaster(CMaster* master);
      void shiftNextSS();
};

class CDevice{
public:
      unsigned int      m_current_position_overall;
      unsigned int      m_num_total_threads;
      unsigned int      m_output_memory_id;
      int               m_input_id;
      double            m_processing_time;
      size_t            m_total_stream_size;
      size_t            m_current_output_size;      
      size_t            m_total_output_size;
      size_t            m_offset_standard;
      bool              m_end_flag;
      bool              m_last_thread;
      char*             m_stream;
      size_t*           m_offset;
      int*              m_output_size;
      int*              m_output_num;
      char*             m_output_stream;
      size_t*           m_output_offset;// gpgpuの計算用に必要事前計算が必要
      void              (*m_count_func)(char*, size_t*, int*, int*);
      void              (*m_func)(char*, size_t*, char*, size_t*);
      std::queue< tagOUTPUT >*
                        m_output_queue;
      std::map<int, unsigned int>*
                        m_input_id_num;//idをキー、その個数がデータ
      CMemory*          m_cpu_memory;
      CMemory*          m_gpu_memory;

      CDevice();
      void setDataFromMaster(CMaster* master);
      void setFunc(void (*count_func)(char*, size_t*, int*, int*), void (*func)(char*, size_t*, char*, size_t*))
      {
            this->m_count_func      = count_func;
            this->m_func            = func;
      }
      void shiftNextSS();
};

#endif /* BSP_H_ */