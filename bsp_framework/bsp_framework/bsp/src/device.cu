#include "bsp.hpp"

__device__ __constant__ size_t offset_standard;
__device__ __constant__ size_t output_offset_standard;

void deviceCount(CDevice* device){
  char* input_stream;
  size_t* input_offset;
  int* output_size, *output_num;

  //実際に実行するスレッド数の確定
  int grid_num      = 1;
  while(128 * grid_num < device->m_num_total_threads)grid_num++;
  dim3 block(128,1,1);
  dim3 grid(grid_num,1,1);

  //グローバル変数offset_standardに値を設定
  CUDA_SAFE_CALL(cudaMemcpyToSymbol(offset_standard, &device->m_offset_standard, sizeof(size_t)));

  //カウント計算
  CUDA_SAFE_CALL(cudaMalloc((void**)&input_stream, device->m_total_stream_size));
  CUDA_SAFE_CALL(cudaMalloc((void**)&input_offset,sizeof(size_t)*128*grid_num));
  CUDA_SAFE_CALL(cudaMalloc((void**)&output_size,sizeof(int)*device->m_num_total_threads));
  CUDA_SAFE_CALL(cudaMalloc((void**)&output_num,sizeof(int)*device->m_num_total_threads));
  CUDA_SAFE_CALL(cudaMemset(output_size, 0, sizeof(int)*device->m_num_total_threads));
  CUDA_SAFE_CALL(cudaMemset(output_num, 0, sizeof(int)*device->m_num_total_threads));
  CUDA_SAFE_CALL(cudaMemset(input_offset, -1, sizeof(size_t)*128*grid_num));
  CUDA_SAFE_CALL(cudaMemcpy(input_stream, device->m_stream, device->m_total_stream_size,cudaMemcpyHostToDevice));
  CUDA_SAFE_CALL(cudaMemcpy(input_offset, device->m_offset, sizeof(size_t)*device->m_num_total_threads,cudaMemcpyHostToDevice));
  device->m_count_func<<<grid,block>>>(input_stream, input_offset, output_size, output_num);
  CUDA_SAFE_CALL(cudaThreadSynchronize() );
  CUDA_SAFE_CALL(cudaMemcpy(device->m_output_size, output_size, sizeof(int)*device->m_num_total_threads, cudaMemcpyDeviceToHost));
  CUDA_SAFE_CALL(cudaMemcpy(device->m_output_num,output_num, sizeof(int)*device->m_num_total_threads, cudaMemcpyDeviceToHost));
  CUDA_SAFE_CALL(cudaFree(input_stream));
  CUDA_SAFE_CALL(cudaFree(input_offset));
  CUDA_SAFE_CALL(cudaFree(output_size));
  CUDA_SAFE_CALL(cudaFree(output_num));
}

void deviceActualProcess(CDevice* device, int thread_offset, int total_thread){
  char* input_stream, *output_stream;
  size_t* input_offset, *output_offset;

  //実際に実行するスレッド数の確定
  int grid_num      = 1;
  while(128 * grid_num < total_thread && grid_num <= 65535)grid_num++;
  if(grid_num > 65535){
    cerr << "the number of threads exceed 128 * 65535 = 8388480" << endl;
    exit(-1);
  }
  dim3 block(128,1,1);
  dim3 grid(grid_num,1,1);

  //グローバル変数offset_standardに値を設定
  CUDA_SAFE_CALL(cudaMemcpyToSymbol(offset_standard, &device->m_offset[thread_offset], sizeof(size_t)));
  CUDA_SAFE_CALL(cudaMemcpyToSymbol(output_offset_standard, &device->m_output_offset[thread_offset], sizeof(size_t)));

  size_t input_stream_size  = device->m_offset[thread_offset + total_thread] - device->m_offset[thread_offset];
  size_t output_stream_size = device->m_output_offset[thread_offset + total_thread] - device->m_output_offset[thread_offset];

  //実計算
  CUDA_SAFE_CALL(cudaMalloc((void**)&output_stream, output_stream_size));
  CUDA_SAFE_CALL(cudaMalloc((void**)&input_stream,  input_stream_size));
  CUDA_SAFE_CALL(cudaMalloc((void**)&output_offset, sizeof(size_t)*total_thread));
  CUDA_SAFE_CALL(cudaMalloc((void**)&input_offset,  sizeof(size_t)*128*grid_num));
  CUDA_SAFE_CALL(cudaMemset(input_offset,   -1, sizeof(size_t)*128*grid_num));
  CUDA_SAFE_CALL(cudaMemcpy(input_stream,   &device->m_stream[device->m_offset[thread_offset] - device->m_offset_standard], input_stream_size,            cudaMemcpyHostToDevice));
  CUDA_SAFE_CALL(cudaMemcpy(input_offset,   &device->m_offset[thread_offset],                                               sizeof(size_t)*total_thread,  cudaMemcpyHostToDevice));
  CUDA_SAFE_CALL(cudaMemcpy(output_offset,  &device->m_output_offset[thread_offset],                                        sizeof(size_t)*total_thread,  cudaMemcpyHostToDevice));
  device->m_func<<<grid,block>>>(input_stream,input_offset,output_stream,output_offset);  
  CUDA_SAFE_CALL(cudaThreadSynchronize() );
  CUDA_SAFE_CALL(cudaMemcpy(&device->m_output_stream[device->m_output_offset[thread_offset]], output_stream, output_stream_size,cudaMemcpyDeviceToHost));
  CUDA_SAFE_CALL(cudaFree(input_stream));
  CUDA_SAFE_CALL(cudaFree(input_offset));
  CUDA_SAFE_CALL(cudaFree(output_offset));
  CUDA_SAFE_CALL(cudaFree(output_stream));
}

__host__ void* deviceProcessing(void* arg){
  auto start        = std::chrono::system_clock::now();
  CDevice* device   = (CDevice*)arg;

  //m_output_sizeとm_output_numにデータを取得
  deviceCount(device);
      // cout << "count end" << endl;


  //カウント計算の結果からアウトプットの準備
  device->m_output_offset[0]      = 0;
  for(int i=0; i<device->m_num_total_threads; i++){
    device->m_output_offset[i+1]  = device->m_output_offset[i] + device->m_output_size[i] + 4*sizeof(int)*device->m_output_num[i];
  }
  device->m_total_output_size = device->m_output_offset[device->m_num_total_threads] + 1024;
  pthread_mutex_lock(&memory_mutex);
  SAFE_MALLOC( device->m_output_stream, char*, device->m_total_output_size);
  device->m_output_memory_id  = device->m_cpu_memory->getSerialNum();
  device->m_cpu_memory->useMemoryForOutput( device->m_output_memory_id, device->m_total_output_size);
  pthread_mutex_unlock(&memory_mutex);

  int thread_offset = 0;
  //カウント計算の結果から分割して処理
  while(thread_offset != device->m_num_total_threads){
    int total_thread                = 0;
    size_t output_offset_standard   = device->m_output_offset[thread_offset];
    #define actual_stream_size device->m_offset[thread_offset + total_thread] - device->m_offset_standard 
    #define actual_output_stream_size device->m_output_offset[thread_offset + total_thread] - output_offset_standard
    #define needed_memory actual_stream_size + actual_output_stream_size + total_thread * 2*sizeof(size_t)
    while(device->m_gpu_memory->isAvailable(needed_memory) && thread_offset + total_thread <= device->m_num_total_threads)total_thread++;
    --total_thread;
    // cout  << "device threads: " << total_thread << endl;
    deviceActualProcess(device, thread_offset, total_thread);
    // cout << "device end" << endl;

    thread_offset   += total_thread;
    //cout << "processing threads: "<<thread_offset << " / " << device->m_num_total_threads << endl;
  }

  //アウトプットの登録
  pushOutputDataToQueueDevice(device);
  pthread_mutex_lock(&memory_mutex);
  device->m_cpu_memory->freeMemory((void*)device->m_output_size);
  device->m_cpu_memory->freeMemory((void*)device->m_output_num);
  device->m_cpu_memory->freeMemory((void*)device->m_output_offset);
  pthread_mutex_unlock(&memory_mutex);
  wakeMasterUp(&device->m_end_flag);

  auto end    = std::chrono::system_clock::now();
  auto diff   = end - start;
  device->m_processing_time += std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}