#include "bsp.hpp"

void InitializeDBcursor(Dbc** , Dbc**, CMaster*);


void ReadFromDocumentsToStream(char* stream, 
                              size_t* offset, 
                              size_t max_stream_size, 
                              unsigned int* max_file_counter, 
                              CMaster* master)
{
  std::ifstream ifs;
  unsigned int file_number = 0;
  size_t file_size = 0;
  long checkpoint = 0;
  std::string file_name, path;

  seekdir(master->m_folder.m_dp, master->m_folder.m_dpoffset);
  dirent* entry = readdir(master->m_folder.m_dp);

  while( entry != NULL ){
    file_name = entry->d_name;
	path = master->m_folder.folder_name + "/" + file_name;
	ifs.open( path.c_str() );
	if( ifs.fail() ){
	  std::cerr<<"fail to read: "<<file_name<<std::endl;
	}else{
	  ifs.seekg(0, ifs.end);
	  file_size = (size_t)ifs.tellg();
	  ifs.seekg(0, ifs.beg);
	  if( offset[file_number] + file_size < max_stream_size && 
	    file_number < *max_file_counter){
	    ifs.read( stream + offset[file_number], file_size );
	    stream[ offset[file_number] + file_size ]   = '\0';
	    offset[ ++file_number ]                     = offset[file_number-1] + (size_t)(file_size + 1);
	    ifs.close();
	  }else{
	    break;
	  }
	}
    master->m_folder.m_dpoffset   = telldir( master->m_folder.m_dp );
    entry                         = readdir( master->m_folder.m_dp );
    if( entry == NULL ){
      master->m_read_end_flag = true;
    }
  }
  *max_file_counter           = file_number;
  master->m_max_stream_size   = offset[file_number];
  return;
}

void copyToStream(char* stream, const void* copy_datap, 
                  size_t* size_offset, size_t copy_size)
{
  memcpy(stream + *size_offset, copy_datap, copy_size);
  *size_offset                  += copy_size;
}

void ReadFromDBToStream(char* stream,
                        size_t* offset,
                        size_t max_stream_size,
                        unsigned int* max_file_counter,
                        CMaster* master)
{
  //DBcursor初期化
  Dbc* cursor1;
  Dbc* cursor2;
  InitializeDBcursor(&cursor1, &cursor2, master);

  //DBからデータをストリームに読み込み
  Dbt key, data;
  int key_size, source_id_size, msg_size;
  char* temp_keyp = NULL; char* temp_datap = NULL;
  std::string store_str;

  int ret = cursor1->get(&key, &data, DB_CURRENT);
  string temp_st((char*)key.get_data(), key.get_size());
  // cout << temp_st << endl;

  unsigned int thread_sum = 0;
  size_t memory_pointer = 0, size_offset = 0, temp_offset = 0;
  size_t needed_memory_for_one_pair   = 4*sizeof(int) + key.get_size() + data.get_size();
  const int minus = -1;

  while(ret != DB_NOTFOUND){
    if(size_offset + needed_memory_for_one_pair > max_stream_size)break;
    temp_keyp = (char*)key.get_data();
    std::string str(temp_keyp, key.get_size());
    if(store_str != str){
      if(!store_str.empty()){
        cursor2->close();
        cursor1->dup(&cursor2, DB_POSITION);
        //-1を境目に入れる
        copyToStream(stream, &minus, &size_offset, sizeof(int));
        //このキー担当のスレッドのオフセットを計算
        offset[thread_sum+1] = size_offset;
        if(++thread_sum >= *max_file_counter)break;
      }
      //今注目するキーをstore_strに保存（新しいキーを設定）
      store_str = str;
      //キーのコピー
      key_size = key.get_size();
      copyToStream(stream, &key_size, &size_offset, sizeof(int));
      copyToStream(stream, temp_keyp, &size_offset, key_size);
    }
    //データのコピー(data.get_size()だと実際のデータサイズと違いあり)
    temp_datap = (char*)data.get_data();
    source_id_size = *(int*)temp_datap;
    msg_size = *(int*)(temp_datap + sizeof(int) + source_id_size);
    copyToStream(stream, temp_datap, &size_offset, sizeof(int)*2 + source_id_size + msg_size);

    ret = cursor1->get(&key, &data, DB_NEXT);
  }

  master->m_db.m_cursor->close();
  master->m_db.m_cursor = NULL;
  if(ret == DB_NOTFOUND){
    // printf("DB end\n");
    master->m_read_end_flag = true;
    copyToStream(stream, &minus, &size_offset, sizeof(int));
    offset[thread_sum+1] = size_offset;
  }else{
    //cursor2にメモリに乗り切らないところからのカーソルが入っているので、次回からはここから
    cursor2->dup(&master->m_db.m_cursor, DB_POSITION);
  }

  cursor1->close();
  cursor2->close();
  *max_file_counter   = thread_sum;
}

void* BspRead(void* arg)
{
  auto start    = std::chrono::system_clock::now();
  CMaster* master   = (CMaster*)arg;

  if( master->m_read_end_flag ){
    wakeMasterUp(&master->m_rw_flag);
    return NULL;
  }

  char* stream = NULL;
  size_t* offset = NULL;
  //必要なメモリサイズ及び実行スレッド数の計算
  pthread_mutex_lock( &memory_mutex );
  size_t available_memory   = master->m_cpu_memory->getInfo();
  size_t stream_size        = master->m_current_input_memory_size;
  unsigned int file_counter = master->m_current_input_thread_size;

  stream = (char*)malloc(stream_size);
  offset = (size_t*)malloc(sizeof(size_t)*file_counter);

  if( stream != NULL && offset != NULL ){
    memset(stream, 0, stream_size);
    memset(offset, 0, sizeof(size_t)*file_counter);
    master->m_cpu_memory->useMemory( stream, stream_size);
    master->m_cpu_memory->useMemory( offset, sizeof(size_t)*file_counter);
    pthread_mutex_unlock( &memory_mutex );

    //用意した配列を用いて各ファイルのサイズ取得、配列への読み出しを行う
    if(master->m_needs_file_read){
      ReadFromDocumentsToStream(stream, offset, stream_size, &file_counter, master);
    }else{
      ReadFromDBToStream(stream, offset, stream_size, &file_counter, master);
    }

    master->setNextInput(file_counter);
  
    if(file_counter != 0){
      tagINPUT input_data = {stream, offset, file_counter};
      master->m_input_queue.push(input_data);
    }else{
      master->m_cpu_memory->freeMemory(stream);
      master->m_cpu_memory->freeMemory(offset);
    }
  }else{
    pthread_mutex_unlock( &memory_mutex );
    free(stream);
    free(offset);
    master->setSmallerInput();
  }
  
  master->m_priority_writing_flag   = true;

  wakeHostUp();
  wakeMasterUp(&master->m_rw_flag);

  auto end    = std::chrono::system_clock::now();
  auto diff   = end - start;
  master->m_read_time += std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}
void Insert(CMaster* master, char* stream, size_t output_size){
  int i                   = 0;
  int max_output_buf_size = 0;
  int dist_id_size        = 0;
  int source_id_size      = 0;
  int msg_size            = 0;
  int msg_num             = 0;
  int ret                 = 0;
  bool need_split         = false;
  bool insert_finished    = false;

  Dbt bulk;
  //何倍必要かわからない（インプットサイズに比例？）
  size_t buff_size  = output_size*2;
  char* bulk_buff = NULL;

  pthread_mutex_lock(&memory_mutex);
  do{
    bulk_buff = (char*)malloc(buff_size);
    if(bulk_buff == NULL)buff_size *= 0.9;
  }while(bulk_buff == NULL);
  master->m_cpu_memory->useMemory( bulk_buff, buff_size);
  pthread_mutex_unlock(&memory_mutex);

  bulk.set_ulen(buff_size);
  bulk.set_data(bulk_buff);

  DbMultipleKeyDataBuilder builder(bulk);

  while( i < output_size ){
    /////////////////////////////////////////////////////////////
    // host->m_insert_streamには、各send毎に
    // int dist_id_size, char* dist_id,
    // int msg_size, char* msg, 
    // int source_id_size, char* source_id,(int blank_for_serial_num)
    // の順でデータが並んでいる
    /////////////////////////////////////////////////////////////
    memcpy(&dist_id_size, stream+i, sizeof(int));
    char* keyp = stream + i + sizeof(int);

    memcpy(&source_id_size, stream+i+sizeof(int)+dist_id_size, sizeof(int));
    memcpy(&msg_size, stream+i+2*sizeof(int)+dist_id_size+source_id_size, sizeof(int));

    if (dist_id_size > 10000 || source_id_size > 10000 || msg_size > 10000)
    {
      cerr<<"Message format error"<<endl;
      exit(-1);
    }

    *(int*)(stream+i+3*sizeof(int)+dist_id_size+source_id_size+msg_size) = master->m_db.m_serial_num++;

    char* datap = stream + i + sizeof(int) + dist_id_size;

    if( (need_split = !builder.append(keyp, dist_id_size, datap, 3*sizeof(int) + source_id_size + msg_size)) == true ){
        if ((ret =  master->m_db.m_db_write->put(NULL, &bulk, NULL, DB_MULTIPLE_KEY)) != 0) {
          master->m_db.m_db_write->err(ret, "Db::put");
          throw DbException(ret);
        }else{
          insert_finished = true;
          pthread_mutex_lock(&memory_mutex);
          master->m_cpu_memory->freeMemory( bulk_buff );
          pthread_mutex_unlock(&memory_mutex);
        }
        Insert(master, &stream[i], output_size - i);
        break;
    }

    //次の処理のinputにはint source_id_size,char* source_id, int msg_size, char* msgで渡す

    i += 4*sizeof(int) + dist_id_size + source_id_size + msg_size;
    msg_num++;
  }

  if(!need_split && !insert_finished){
    if ((ret =  master->m_db.m_db_write->put(NULL, &bulk, NULL, DB_MULTIPLE_KEY)) != 0) {
       master->m_db.m_db_write->err(ret, "Db::put");
       throw DbException(ret);
    }else{
      pthread_mutex_lock(&memory_mutex);
      master->m_cpu_memory->freeMemory( bulk_buff );
      pthread_mutex_unlock(&memory_mutex);
    }
  }

}

__host__ void* TempDBInsert(void* tag)
{
  auto start      = std::chrono::system_clock::now();
  CMaster* master = (CMaster*)tag;
  pthread_mutex_lock(&output_queue_mutex);
  
  int queue_size = master->m_output_queue.size();
  dump(queue_size);
  
  if(master->m_output_queue.empty()){
    if(master->m_read_end_flag && master->m_process_end_flag){
      master->m_write_end_flag = true;
    }else if(master->m_read_end_flag && !master->m_process_end_flag){
      pthread_mutex_unlock(&output_queue_mutex);//他のスレッドがm_process_end_flagをいじるのでロックが必要
      pthread_mutex_lock(&rw_mutex);
      dump("insert sleep");
      pthread_cond_wait(&rw_cond, &rw_mutex);
      pthread_mutex_unlock(&rw_mutex);
    }else if(!master->m_read_end_flag){
      master->m_priority_writing_flag = false;
    }
    pthread_mutex_unlock(&output_queue_mutex);
    wakeMasterUp(&master->m_rw_flag);
    return NULL;
  }
  tagOUTPUT* output_data   = &master->m_output_queue.front();
  pthread_mutex_unlock(&output_queue_mutex);

  char* stream                      = output_data->stream;
  size_t total_output_size          = output_data->size;

  //実インサート部
  Insert(master, stream, total_output_size);

  //stream解放
  pthread_mutex_lock(&memory_mutex);
  SAFE_FREE(stream);
  master->m_cpu_memory->freeMemoryForOutput( output_data->output_memory_id );
  pthread_mutex_unlock(&memory_mutex);

  //output_queueからデータ除去
  pthread_mutex_lock(&output_queue_mutex);
  master->m_output_queue.pop();
  pthread_mutex_unlock(&output_queue_mutex);


  auto end    = std::chrono::system_clock::now();
  auto diff   = end - start;
  master->m_write_time += std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

  wakeMasterUp(&master->m_rw_flag);
}

void InitializeDBcursor(Dbc** cursor1, Dbc** cursor2, CMaster* master)
{
  int ret=0;
  Dbt key, data;
  Dbc* temp_cursor;
  if(master->m_db.m_cursor == NULL){
    master->m_db.m_db_read->cursor(NULL, &temp_cursor, 0);
    ret = temp_cursor->get(&key, &data, DB_FIRST);
    if(ret != DB_NOTFOUND){
      temp_cursor->dup(cursor1, DB_POSITION);
      temp_cursor->dup(cursor2, DB_POSITION);
      temp_cursor->dup(&master->m_db.m_cursor, DB_POSITION);
    }else{
      std::cout << "DB get error" << std::endl;
      exit(-1);
    }
    temp_cursor->close();
    temp_cursor = NULL;
  }else{
    master->m_db.m_cursor->dup(cursor1, DB_POSITION);
    master->m_db.m_cursor->dup(cursor2, DB_POSITION); 
  }
}