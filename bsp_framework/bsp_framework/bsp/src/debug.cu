#include "bsp.hpp"

pthread_mutex_t debug_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

queue<string> argment_contents;

void set_args_con(){};

string gen_string(string s) {
    s+=',';
    string ret="";
    int par=0;
    for(int i=0; i<(int)s.size(); i++) {
        if(s[i]=='(' || s[i]=='<' || s[i]=='{') par++;
        else if(s[i]==')' || s[i]=='>' || s[i]=='}') par--;
        if(par==0 && s[i]==',') {
        	pthread_mutex_lock(&debug_queue_mutex);
            ret+="="+argment_contents.front();
            argment_contents.pop();
            pthread_mutex_unlock(&debug_queue_mutex);
            if(i!=(int)s.size()-1) {
                ret+=",";
            }
        }
        else ret+=s[i];
    }
    return ret;
}