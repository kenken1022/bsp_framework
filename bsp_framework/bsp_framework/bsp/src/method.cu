#include "bsp.hpp"

///////////////////////////////////////////////////////////////////////////////////////
//CMemory//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
CMemory::CMemory(){
  this->m_serial_num      = 0;
  this->m_max_memory 		  = 0;
  this->m_current_memory  = 0;
  m_memory_consumption        = new unordered_map<void*,size_t>[1];
  m_output_memory_consumption = new unordered_map<unsigned int, size_t>[1];
}
void CMemory::setMemoryMax( size_t max ){
    this->m_max_memory = max;
    this->m_current_memory = max;
}
bool CMemory::isAvailable( size_t memory)
{
	if(this->m_current_memory >= memory + (size_t)ALLOWANCE)return true;
  else return false;
}
bool CMemory::useMemory( void* pointer, size_t memory )
{
  if( this->m_current_memory >= memory + ALLOWANCE ){
    (*m_memory_consumption)[pointer] = (size_t)memory;
    m_current_memory -= memory;
    return true;
  }else{
    std::cout<<"There is not enough memory to use for \""<<pointer<<"\""<<std::endl;
    return false;
  }
}
bool CMemory::useMemoryForOutput( unsigned int id, size_t memory)
{
  if( this->m_current_memory >= memory + ALLOWANCE ){
    (*m_output_memory_consumption)[id] = memory;
    this->m_current_memory -= memory;
    return true;
  }else{
    std::cout<<"There is not enough memory to use for \""<<id<<"\""<<std::endl;
    return false;
  }
}
bool CMemory::addMemory( void* pointer, size_t memory )
{
	size_t current_memory = (*m_memory_consumption)[pointer];
	if( current_memory != 0 ){
		this->m_current_memory                  -= memory;
		(*m_memory_consumption)[pointer] += memory;
		return true;
	}else{
		std::cout<<"There is no memory consumption named \""<< pointer <<"\"."<<std::endl;
		return false;
	}
}
bool CMemory::freeMemory( void* pointer )
{
  size_t memory = (*m_memory_consumption)[pointer];
  if( memory != 0 ){
        this->m_current_memory += memory;
        SAFE_FREE(pointer);
        (*m_memory_consumption).erase(pointer);
        return true;
  }else{
        std::cout<<"There is no memory consumption named \""<< pointer <<"\"."<<std::endl;
        return false;
  }
}
bool CMemory::freeMemoryForOutput(unsigned int id)
{
  size_t memory = (*m_output_memory_consumption)[id];
  if( memory != 0 ){
    this->m_current_memory += memory;
    (*m_output_memory_consumption).erase(id);
  }else{
    std::cout<<"There is no memory consumption for \""<< id <<"\"."<<std::endl;
    return false;
  }
}
size_t CMemory::getInfo()
{
  return this->m_current_memory;
}
unsigned int CMemory::getSerialNum(){
  return this->m_serial_num++;
}
CMemory::~CMemory()
{
    SAFE_DELETE(m_memory_consumption);
    SAFE_DELETE(m_output_memory_consumption);
}
///////////////////////////////////////////////////////////////////////////////////////
//CMemory//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
//CDBSet//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
/*
//もともとの実装、データベースを空にするとエラーになってしまったため現在は配列でDBを用意
CDBset::CDBset(): 
  m_rmEnv((u_int32_t)0),
  m_myEnv((u_int32_t)0),
  m_temp_db0(&m_myEnv, 0),
  m_temp_db1(&m_myEnv, 0)
{
    m_serial_num    = 0;
    m_db_num        = 2;
    m_cursor        = NULL;

    u_int32_t oFlags    = DB_CREATE;
    u_int32_t env_flags = DB_CREATE     |  // If the environment does not
                                         // exist, create it.
                        //DB_INIT_LOG   |  // Initialize logging
                        DB_INIT_MPOOL;  // Initialize the cache

    std::string envHome("/etc/db");
  
    m_rmEnv.remove(envHome.c_str(), DB_FORCE);//removeしてるのでcloseは不要
    try {
      m_myEnv.set_cachesize(0, 1000 * 1024 * 1024, 1);
      m_myEnv.set_flags(DB_TXN_WRITE_NOSYNC, 1);
      m_myEnv.set_flags(DB_TXN_NOSYNC, 1);
      m_myEnv.open(envHome.c_str(), env_flags, 0);
    } catch(DbException &e) {
      std::cerr << "Error opening database and environment: "
                << envHome << std::endl;
      std::cerr << e.what() << std::endl;
    }

    try{
      remove("/etc/db/temp_db0.db");
      m_temp_db0.set_flags(DB_DUPSORT);
      m_temp_db0.open(NULL,"/etc/db/temp_db0.db",NULL,DB_BTREE,oFlags,0);
      remove("/etc/db/temp_db1.db");
      m_temp_db1.set_flags(DB_DUPSORT);
      m_temp_db1.open(NULL,"/etc/db/temp_db1.db",NULL,DB_BTREE,oFlags,0);                  
    }catch(DbException &e){
      std::cerr<<"DB open error"<<std::endl;
    }catch(std::exception &e){
      std::cerr<<"some errors occured when DB opening"<<std::endl;
    }

    m_db_write  = &m_temp_db0;
    m_db_read   = &m_temp_db1;
}*/

CDBset::CDBset(): 
  m_rmEnv((u_int32_t)0),
  m_myEnv((u_int32_t)0)
{

    for(int i=0; i< 100; i++){
      new(m_temp_db + i) CDB(&m_myEnv);
    }

    m_serial_num    = 0;
    m_db_num        = 0;
    m_cursor        = NULL;

    u_int32_t oFlags    = DB_CREATE;
    u_int32_t env_flags = DB_CREATE     |  // If the environment does not
                                         // exist, create it.
                        //DB_INIT_LOG   |  // Initialize logging
                        DB_INIT_MPOOL;  // Initialize the cache

    mkdir("/etc/db", 0777);
    std::string envHome("/etc/db");
  
    m_rmEnv.remove(envHome.c_str(), DB_FORCE);//removeしてるのでcloseは不要
    try {
      m_myEnv.set_cachesize(0, 1000 * 1024 * 1024, 1);
      m_myEnv.set_flags(DB_TXN_WRITE_NOSYNC, 1);
      m_myEnv.set_flags(DB_TXN_NOSYNC, 1);
      m_myEnv.open(envHome.c_str(), env_flags, 0);
    } catch(DbException &e) {
      std::cerr << "Error opening database and environment: "
                << envHome << std::endl;
      std::cerr << e.what() << std::endl;
    }

    try{
      for(int i=0; i<100; i++){
        m_temp_db[i].db.set_flags(DB_DUPSORT);
        string db_name = "/etc/db/temp_db"+to_string(i)+".db";
        remove(db_name.c_str());
        m_temp_db[i].db.open(NULL, db_name.c_str(),NULL,DB_BTREE,oFlags,0);
      }
    }catch(DbException &e){
      std::cerr<<"DB open error"<<std::endl;
      exit(-1);
    }catch(std::exception &e){
      std::cerr<<"some errors occured when DB opening"<<std::endl;
      exit(-1);
    }

    m_db_write  = &(m_temp_db[0].db);
    m_db_read   = NULL;
}

CDBset::~CDBset()
{
  try {
    m_db_write->close(0);
    m_db_read->close(0);
    m_myEnv.close((u_int32_t)0);
  }catch(DbException &e){
    std::cerr << "Error closing database and environment" << std::endl;
    std::cerr << e.what() << std::endl;
  }catch(std::exception &e){
    std::cerr<<"some errors occured when DB opening"<<std::endl;
  }  
}
///////////////////////////////////////////////////////////////////////////////////////
//CDBSet//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
//CFolder//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
CFolder::CFolder(){
  this->m_dpoffset          = 0;
  this->m_entry             = NULL;
  this->m_dp                = NULL;
  this->Open();
}
CFolder::CFolder(std::string folder_name){
  this->m_dpoffset          = 0;
  this->m_entry             = NULL;
  this->m_dp                = NULL;
  this->folder_name         = folder_name;
  this->Open();
}
void CFolder::Clear(){
  this->m_dpoffset          = 0;
  m_entry                   = NULL;
  rewinddir(m_dp);
}
bool CFolder::Open(){
  if( !folder_name.empty() ){
    m_dp        = opendir( folder_name.c_str() );
    if(m_dp == NULL){
      std::cerr<<"can't open folder"<<std::endl;
      exit(-1);
    }
    m_dpoffset  = telldir(m_dp);
    return true;
  }else{
    std::cerr<<"Folder name hasn't been set"<<std::endl;
    exit(-1);
  }
}
CFolder::~CFolder(){
  closedir(m_dp);
  folder_name.clear();
}
///////////////////////////////////////////////////////////////////////////////////////
//CFolder//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
//CMaster//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
CMaster::CMaster(std::string folder_name):
  m_folder(folder_name),
  m_db()
{
  m_current_position                  = 0;
  m_current_position_overall          = 0;
  m_current_input_memory_size         = 1024*1024*2;
  m_current_input_thread_size         = 1000;
  m_input_id                          = 0;
  m_allocation_host_time              = 0;
  m_allocation_device_time            = 0;
  m_read_time                         = 0;
  m_write_time                        = 0;
  m_current_stream_offset             = 0;
  m_priority_writing_flag             = true;
  m_read_end_flag                     = false;
  m_process_end_flag                  = false;
  m_write_end_flag                    = false;
  m_rw_flag                           = false;
  m_needs_file_read                   = true;
  SAFE_NEW( m_cpu_memory, CMemory, 1 );
  SAFE_NEW( m_gpu_memory, CMemory, 1 );
  MemorySet();
  m_input_id_num.insert(make_pair(0, 0));
  m_input_id_finished.insert(make_pair(0, false));
}

void CMaster::MemorySet()
{
  #define MB 1024*1024
  size_t cpu_memory_size = 100;
  char* for_cpu_memory = NULL;

  do{
        free(for_cpu_memory);
        for_cpu_memory = (char*)malloc(cpu_memory_size * MB);
        cpu_memory_size += 100;
  }while(for_cpu_memory != NULL);
  free(for_cpu_memory);

  size_t device_free_memory, device_max_memory;
  CUDA_SAFE_CALL( cudaMemGetInfo(&device_free_memory, &device_max_memory) );

  m_cpu_memory->setMemoryMax(cpu_memory_size*MB);
  m_gpu_memory->setMemoryMax(device_free_memory);
/*
  //for test
  size_t cpu_mem_size = TEST_CPU_MEMORY;
  cpu_mem_size *= MB;
  size_t gpu_mem_size = TEST_GPU_MEMORY;
  gpu_mem_size *= MB;
  m_cpu_memory->setMemoryMax( cpu_mem_size );
  m_gpu_memory->setMemoryMax( gpu_mem_size );
*/
  

  m_cpu_memory->getInfo();
  m_gpu_memory->getInfo();
}
void CMaster::shiftNextSS()
{
  m_current_position            = 0;
  m_current_position_overall    = 0;
  m_current_input_memory_size   = 1024*1024*2;
  m_current_input_thread_size   = 1000;
  m_input_id                    = 0;
  m_allocation_host_time        = 0;
  m_allocation_device_time      = 0;
  m_current_stream_offset       = 0;
  m_priority_writing_flag       = true;
  m_read_end_flag               = false;
  m_process_end_flag            = false;
  m_write_end_flag              = false;
  m_rw_flag                     = false;
  m_needs_file_read             = false;
  m_db.shiftNextSS();
  if(!m_input_queue.empty())cout << "input_queue is not empty" << endl;
  if(!m_output_queue.empty()){
    cout << "output_queue is not empty" << endl;
    cout << m_output_queue.size() << endl;
  }
  m_input_id_num.clear();
  m_input_id_finished.clear();
  m_input_id_num.insert(make_pair(0, 0));
  m_input_id_finished.insert(make_pair(0, false));
}
void CMaster::setHostProcessData(CHost* host)
{
  // cout << "//\n//setHostProcessData\n//" << endl;
  auto start = std::chrono::system_clock::now();
  tagINPUT* input_data;
  if(!this->m_input_queue.empty()){
    input_data   = &this->m_input_queue.front();
  }else{
    // cout << "Input queue is empty" << endl;
    return;
  }

  #define UNIFORMLY_SPLIT input_data->thread_num / (cpu_processing_thread+1)

  unsigned int needed_thread      = input_data->thread_num - this->m_current_position;
  dump("host", needed_thread);

  if(needed_thread <= DEFAULT_THREAD_NUM){
    host->m_num_total_threads     = needed_thread;
    //ここでinput_queueの中のデータを解放するとスレッドが使えないためここではpopだけ
    this->m_input_queue.pop();
    if(this->m_input_queue.empty() && this->m_read_end_flag){//popした結果空？
      host->m_last_thread = true;
    }
  }else{
    host->m_num_total_threads   = DEFAULT_THREAD_NUM;
  }

  host->m_input_id              = this->m_input_id;
  pthread_mutex_lock(&input_id_num_mutex);
  this->m_input_id_num.insert(make_pair(host->m_input_id, this->m_input_id_num.at(host->m_input_id)+1));
  pthread_mutex_unlock(&input_id_num_mutex);

  host->m_offset_standard       = input_data->offset[this->m_current_position];
  host->m_total_stream_size     = input_data->offset[this->m_current_position + host->m_num_total_threads] - host->m_offset_standard;
  host->m_current_output_size   = 0;
  host->m_total_output_size     = DEFAULT_OUTPUT_SIZE;

  host->m_offset          = &input_data->offset[this->m_current_position]; 
  host->m_stream          = &input_data->stream[host->m_offset_standard];
  host->m_output_stream   = NULL;

  pthread_mutex_lock(&memory_mutex);
  SAFE_MALLOC( host->m_output_stream, char*, host->m_total_output_size);
  host->m_output_memory_id     = host->m_cpu_memory->getSerialNum();
  host->m_cpu_memory->useMemoryForOutput( host->m_output_memory_id, host->m_total_output_size );
  pthread_mutex_unlock(&memory_mutex);

  if(needed_thread <= DEFAULT_THREAD_NUM){
    this->m_current_position      = 0;
    if(this->m_output_queue.size() <= 1000)this->m_priority_writing_flag = false;
    tagFREE free_data             = {*input_data, this->m_input_id};
    this->m_free_queue.push(free_data);
    this->m_input_id_finished.insert(make_pair(this->m_input_id, true));
    this->m_input_id++;
    this->m_input_id_finished.insert(make_pair(this->m_input_id, false));
    pthread_mutex_lock(&input_id_num_mutex);
    this->m_input_id_num.insert(make_pair(this->m_input_id, 0));
    pthread_mutex_unlock(&input_id_num_mutex);
  }else{
    this->m_current_position      += host->m_num_total_threads;
  }
  auto end = std::chrono::system_clock::now();
  auto diff = end - start;
  this->m_allocation_host_time += std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
}    
void CMaster::setDeviceProcessData(CDevice* device)
{
    auto start = std::chrono::system_clock::now();

  tagINPUT* input_data;
  if(!this->m_input_queue.empty()){
    input_data   = &this->m_input_queue.front();
  }else{
    return;
  }

  unsigned int needed_thread      = input_data->thread_num - this->m_current_position;
  dump("device", needed_thread);

  device->m_offset_standard       = input_data->offset[this->m_current_position];
  unsigned int total_thread       = 0;

  device->m_num_total_threads = DEFAULT_THREAD_NUM_DEVICE;
  if(needed_thread <= device->m_num_total_threads){
    device->m_num_total_threads     = needed_thread; 
    this->m_input_queue.pop();
    if(this->m_input_queue.empty() && this->m_read_end_flag){
      device->m_last_thread = true;
    }
  }

  device->m_input_id              = this->m_input_id;
  pthread_mutex_lock(&input_id_num_mutex);
  this->m_input_id_num.insert(make_pair(device->m_input_id, this->m_input_id_num.at(device->m_input_id)+1));
  pthread_mutex_unlock(&input_id_num_mutex);

  device->m_total_stream_size     = input_data->offset[this->m_current_position + device->m_num_total_threads] - device->m_offset_standard;
  device->m_current_output_size   = 0;
  device->m_total_output_size     = 0;

  device->m_offset          = &input_data->offset[this->m_current_position];
  device->m_stream          = &input_data->stream[input_data->offset[this->m_current_position]];
  device->m_output_stream   = NULL;

  pthread_mutex_lock(&memory_mutex);
  SAFE_MALLOC( device->m_output_size, int*, sizeof(int)*device->m_num_total_threads);
  this->m_cpu_memory->useMemory( (void*)device->m_output_size, sizeof(int)*device->m_num_total_threads);
  SAFE_MALLOC( device->m_output_num, int*, sizeof(int)*device->m_num_total_threads);
  this->m_cpu_memory->useMemory( (void*)device->m_output_num, sizeof(int)*device->m_num_total_threads);
  SAFE_MALLOC( device->m_output_offset, size_t*, sizeof(size_t)*(device->m_num_total_threads+1) );
  this->m_cpu_memory->useMemory( (void*)device->m_output_offset, sizeof(size_t)*(device->m_num_total_threads+1));
  pthread_mutex_unlock(&memory_mutex);

  if(needed_thread <= device->m_num_total_threads){
    this->m_current_position      = 0;
    this->m_priority_writing_flag = false;
    tagFREE free_data             = {*input_data, this->m_input_id};
    this->m_free_queue.push(free_data);
    this->m_input_id_finished.insert(make_pair(this->m_input_id, true));
    this->m_input_id++;
    this->m_input_id_finished.insert(make_pair(this->m_input_id, false));
    pthread_mutex_lock(&input_id_num_mutex);
    this->m_input_id_num.insert(make_pair(this->m_input_id, 0));
    pthread_mutex_unlock(&input_id_num_mutex);
  }else{
    this->m_current_position      += device->m_num_total_threads;
  }
  auto end = std::chrono::system_clock::now();
  auto diff = end - start;
  this->m_allocation_device_time += std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
}

void CMaster::freeInput(){
  tagFREE free_data   = this->m_free_queue.front();
  pthread_mutex_lock(&input_id_num_mutex);
  if(this->m_input_id_num.at(free_data.id) == 0 && this->m_input_id_finished.at(free_data.id)){
    pthread_mutex_lock(&memory_mutex);
    this->m_cpu_memory->freeMemory(free_data.input_data.stream);
    this->m_cpu_memory->freeMemory(free_data.input_data.offset);
    this->m_free_queue.pop();
    pthread_mutex_unlock(&memory_mutex);
  }
  pthread_mutex_unlock(&input_id_num_mutex);
}

void CMaster::setNextInput(unsigned int thread){
  if(thread == this->m_current_input_thread_size){
    this->m_current_input_thread_size *= 2;
    // this->m_current_input_thread_size += 2000;
  }else{
    this->m_current_input_memory_size *= 2;
    // this->m_current_input_memory_size += 1024*1024*4;
  }
}

void CMaster::setSmallerInput(){
  if(this->m_current_input_thread_size / 2 > 1000)this->m_current_input_thread_size /= 2;
  if(this->m_current_input_memory_size / 2 > 1024*1024*2)this->m_current_input_memory_size /= 2;
}
///////////////////////////////////////////////////////////////////////////////////////
//CMaster//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
//CHost//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
CHost::CHost(){
  this->m_current_position_overall  = 0;
  this->m_num_total_threads         = 0;
  this->m_input_id                  = 0;
  this->m_processing_time           = 0;
  this->m_total_stream_size         = 0;
  this->m_current_output_size       = 0;
  this->m_total_output_size         = 0;
  this->m_offset_standard           = 0;
  this->m_end_flag      = false;
  this->m_last_thread   = false;
  this->m_stream        = NULL;
  this->m_offset        = NULL;
}
void CHost::setDataFromMaster(CMaster* master){
  this->m_output_queue  = &master->m_output_queue;
  this->m_input_id_num  = &master->m_input_id_num;
  this->m_cpu_memory    = master->m_cpu_memory;
}
void CHost::shiftNextSS(){
  this->m_current_position_overall  = 0;
  this->m_num_total_threads         = 0;
  this->m_input_id                  = 0;
  this->m_processing_time           = 0;
  this->m_total_stream_size         = 0;
  this->m_current_output_size       = 0;
  this->m_total_output_size         = 0;
  this->m_offset_standard           = 0;
  this->m_end_flag                  = false;
  this->m_last_thread               = false;
  this->m_stream                    = NULL;
  this->m_offset                    = NULL;
}

///////////////////////////////////////////////////////////////////////////////////////
//CHost//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
//CDevice//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
CDevice::CDevice(){
  this->m_current_position_overall  = 0;
  this->m_num_total_threads         = 0;
  this->m_input_id                  = 0;
  this->m_processing_time           = 0;
  this->m_total_stream_size         = 0;
  this->m_current_output_size       = 0;
  this->m_total_output_size         = 0;
  this->m_offset_standard           = 0;
  this->m_end_flag      = false;
  this->m_last_thread   = false;
  this->m_stream        = NULL;
  this->m_offset        = NULL;
  this->m_output_size   = NULL;
  this->m_output_num    = NULL;
  this->m_output_stream = NULL;
  this->m_output_offset = NULL;
  this->m_count_func    = NULL;
  this->m_func          = NULL;
}
void CDevice::setDataFromMaster(CMaster* master){
  this->m_output_queue  = &master->m_output_queue;
  this->m_input_id_num  = &master->m_input_id_num;
  this->m_cpu_memory    = master->m_cpu_memory;
  this->m_gpu_memory    = master->m_gpu_memory;
}
void CDevice::shiftNextSS(){
  this->m_current_position_overall  = 0;
  this->m_num_total_threads         = 0;
  this->m_input_id                  = 0;
  this->m_processing_time           = 0;
  this->m_total_stream_size         = 0;
  this->m_current_output_size       = 0;
  this->m_total_output_size         = 0;
  this->m_offset_standard           = 0;
  this->m_end_flag      = false;
  this->m_last_thread   = false;
  this->m_stream        = NULL;
  this->m_offset        = NULL;
  this->m_output_size   = NULL;
  this->m_output_num    = NULL;
  this->m_output_stream = NULL;
  this->m_output_offset = NULL;
  this->m_count_func    = NULL;
  this->m_func          = NULL;
}

///////////////////////////////////////////////////////////////////////////////////////
//CDevice//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

