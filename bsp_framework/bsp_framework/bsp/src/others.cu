#include "bsp.hpp"

CMemory cpu_memory;
CMemory gpu_memory;

__host__ void* hostSleep(void* arg){
  dump("host sleep");
  CHost* host = (CHost*)arg;
  pthread_mutex_lock(&host_mutex);
  while(!wake_host_flag)pthread_cond_wait( &host_cond, &host_mutex );
  host->m_end_flag = true;
  pthread_mutex_unlock(&host_mutex);
  dump("host wake");
}

__host__ void* deviceSleep(void* arg){
  dump("device sleep");
  CDevice* device   = (CDevice*)arg;
  pthread_mutex_lock(&host_mutex);
  while(!wake_host_flag)pthread_cond_wait( &host_cond, &host_mutex );
  device->m_end_flag = true;
  pthread_mutex_unlock(&host_mutex);
  dump("device wake");
}

void wakeMasterUp(bool* flag){
  pthread_mutex_lock(&master_mutex);
  *flag = true;
  pthread_cond_broadcast(&master_cond);
  pthread_mutex_unlock(&master_mutex);
}

void wakeHostUp(){
  pthread_mutex_lock(&host_mutex);
  wake_host_flag = true;
  pthread_cond_broadcast(&host_cond);
  pthread_mutex_unlock(&host_mutex);
}

void pushOutputDataToQueueHost(CHost* host){
  tagOUTPUT output_pair   = {host->m_output_memory_id, host->m_output_stream, host->m_current_output_size};
  pthread_mutex_lock(&output_queue_mutex);
  host->m_output_queue->push(output_pair);
  pthread_mutex_unlock(&output_queue_mutex);

  pthread_mutex_lock(&input_id_num_mutex);
  host->m_input_id_num->insert(make_pair(host->m_input_id, host->m_input_id_num->at(host->m_input_id)-1));
  pthread_mutex_unlock(&input_id_num_mutex);

  pthread_mutex_lock(&rw_mutex);
  pthread_cond_broadcast(&rw_cond);
  pthread_mutex_unlock(&rw_mutex);
}

void pushOutputDataToQueueDevice(CDevice* device){
  #define device_output_stream_size device->m_output_offset[device->m_num_total_threads]
  tagOUTPUT output_pair   = {device->m_output_memory_id, device->m_output_stream, device_output_stream_size};
  pthread_mutex_lock(&output_queue_mutex);
  device->m_output_queue->push(output_pair);
  pthread_mutex_unlock(&output_queue_mutex);

  pthread_mutex_lock(&input_id_num_mutex);
  device->m_input_id_num->insert(make_pair(device->m_input_id, device->m_input_id_num->at(device->m_input_id)-1));
  pthread_mutex_unlock(&input_id_num_mutex);

  pthread_mutex_lock(&rw_mutex);
  pthread_cond_broadcast(&rw_cond);
  pthread_mutex_unlock(&rw_mutex);
}

__host__ void BspMemSetSize_host(int* output_size_buf,
                                    int* output_num_buf,
                                    int dist_id_size, 
                                    int msg_size,
                                    int source_id_size){
  int one_msg_set_size;
  if(dist_id_size != 0 && dist_id_size < 4){
    one_msg_set_size = source_id_size + 4 + msg_size;
  }else{
    one_msg_set_size = source_id_size + dist_id_size + msg_size;
  }
  *output_size_buf += one_msg_set_size;
  *output_num_buf += 1;
  return;
}

__device__ void BspMemSetSize_device(int* output_size_buf,
                                    int* output_num_buf,
                                    int dist_id_size, 
                                    int msg_size,
                                    int source_id_size){
  int one_msg_set_size;
  one_msg_set_size = source_id_size + dist_id_size + msg_size;
  *output_size_buf += one_msg_set_size;
  *output_num_buf += 1;
  return;
}

//char**にしないと、関数内でoutput_streamの位置を変えてももどってしまう
__host__ void BspSend_host(CHost* host,
                              int dist_id_size, char* dist_id,
                              int msg_size, char* msg,
                              int source_id_size, char* source_id ){
  if(host->m_current_output_size + 4*sizeof(int) + dist_id_size + source_id_size + msg_size 
    > host->m_total_output_size){
    //cout << "output memory is not enough" << endl;
    pushOutputDataToQueueHost(host);
    host->m_total_output_size   = DEFAULT_OUTPUT_SIZE;
    

    pthread_mutex_lock(&memory_mutex);
    SAFE_MALLOC(host->m_output_stream, char*, host->m_total_output_size);
    host->m_output_memory_id = host->m_cpu_memory->getSerialNum();
    host->m_cpu_memory->useMemoryForOutput( host->m_output_memory_id, host->m_total_output_size);
    pthread_mutex_unlock(&memory_mutex);
    host->m_current_output_size = 0;
  }
  memcpy(host->m_output_stream + host->m_current_output_size, &dist_id_size, sizeof(int));
  memcpy(host->m_output_stream + host->m_current_output_size + sizeof(int), dist_id, sizeof(char)*dist_id_size);
  memcpy(host->m_output_stream + host->m_current_output_size + sizeof(int) + dist_id_size, &source_id_size, sizeof(int));
  if(source_id != NULL)memcpy(host->m_output_stream + host->m_current_output_size + 2*sizeof(int) + dist_id_size, source_id, sizeof(char)*source_id_size);
  memcpy(host->m_output_stream + host->m_current_output_size + 2*sizeof(int) + dist_id_size + source_id_size, &msg_size, sizeof(int));
  memcpy(host->m_output_stream + host->m_current_output_size + 3*sizeof(int) + dist_id_size + source_id_size, msg, sizeof(char)*msg_size);
  host->m_current_output_size += 4*sizeof(int) + dist_id_size + source_id_size + msg_size;
  //messageの最後に通し番号としてint型整数を入れるため4*sizeof(int)
}

__device__ void BspSend_device(char** output_stream, 
                              int dist_id_size, char* dist_id,
                              int msg_size, char* msg,
                              int source_id_size, char* source_id){
  memcpy(*output_stream, &dist_id_size, sizeof(int));
  memcpy(*output_stream + sizeof(int), dist_id, sizeof(char)*dist_id_size);
  memcpy(*output_stream + sizeof(int) + dist_id_size, &source_id_size, sizeof(int));
  if(source_id != NULL)memcpy(*output_stream + 2*sizeof(int) + dist_id_size, source_id, sizeof(char)*source_id_size);
  memcpy(*output_stream + 2*sizeof(int) + dist_id_size + source_id_size, &msg_size, sizeof(int));
  memcpy(*output_stream + 3*sizeof(int) + dist_id_size + source_id_size, msg, sizeof(char)*msg_size);
  *output_stream += 4*sizeof(int) + dist_id_size + source_id_size + msg_size;

  return;
}

__host__ int BspGetIDSize_host(char* str){
  int ID_size = *(int*)str;
  return ID_size;
}

__device__ int BspGetIDSize_device(char* str){
  int ID_size;
  memcpy(&ID_size, str, sizeof(int));
  return ID_size;
}

__host__ char* BspGetIDPos_host(char* str){
  return str + sizeof(int);
}

__device__ char* BspGetIDPos_device(char* str){
  return str + sizeof(int);
}

__host__ char* BspFirstMsg_host(char* str){
  int ID_size = *(int*)str;
  char* first_msg = str + sizeof(int) + ID_size;
  return first_msg;
}

__device__ char* BspFirstMsg_device(char* str){
  int ID_size;
  memcpy(&ID_size, str, sizeof(int));
  char* first_msg = str + sizeof(int) + ID_size;
  return first_msg;
}

//msgとsource直す必要あり
__host__ void BspNextMsg_host(char** msg_pointer){
  int source_id_size, msg_size;
  source_id_size = *(int*)*msg_pointer;
  msg_size = *(int*)(*msg_pointer + sizeof(int) + source_id_size);
  *msg_pointer += 2*sizeof(int) + source_id_size + msg_size;
  if(*((int*)*msg_pointer) == -1)*msg_pointer = NULL;
}

__device__ void BspNextMsg_device(char** msg_pointer){
  int source_id_size, msg_size;
  //hostのように代入するとmisalignedになる
  memcpy(&source_id_size, *msg_pointer,                                 sizeof(int));
  memcpy(&msg_size,       *msg_pointer + sizeof(int) + source_id_size,  sizeof(int));
  //for(int i=0; i<2*sizeof(int)+source_id_size+msg_size; i++)(*msg_pointer)++;
  (*msg_pointer) += 2*sizeof(int)+source_id_size+msg_size;
  int pointer_num = 0;
  memcpy(&pointer_num, *msg_pointer, sizeof(int));
  if(pointer_num == -1)*msg_pointer = NULL;
}
